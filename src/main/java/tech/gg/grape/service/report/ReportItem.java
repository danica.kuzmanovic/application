package tech.gg.grape.service.report;

import javax.persistence.Enumerated;
import java.time.LocalDate;

public class ReportItem {

    @Enumerated
    private Operations operation;

    private long operationId;

    private LocalDate date;

    public ReportItem(Operations operation, long operationId, LocalDate date) {
        this.operation = operation;
        this.operationId = operationId;
        this.date = date;
    }

    public Operations getOperation() {
        return operation;
    }

    public void setOperation(Operations operation) {
        this.operation = operation;
    }

    public long getOperationId() {
        return operationId;
    }

    public void setOperationId(long operationId) {
        this.operationId = operationId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
