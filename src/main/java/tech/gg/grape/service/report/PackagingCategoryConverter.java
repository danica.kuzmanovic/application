package tech.gg.grape.service.report;

import tech.gg.grape.repository.model.PackagingCategory;

public class PackagingCategoryConverter {

    public static String convert(PackagingCategory packagingCategory) {
        switch (packagingCategory) {
            case BOTTLE:
                return "Flaša";
            case BOTTLE_CAP:
                return "Zatvarač";
            case LABEL:
                return "Etiketa";
            case DECORATIVE_CAP:
                return "Ukrasne kapise";
        }
        return "";
    }
}
