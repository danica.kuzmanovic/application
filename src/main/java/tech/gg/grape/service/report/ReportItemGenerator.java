package tech.gg.grape.service.report;

import tech.gg.grape.service.service.StorageSpentService;
import tech.gg.grape.repository.model.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ReportItemGenerator {

    private StorageSpentService storageSpentService;

    private List<SpentMaterial> spentMaterials;
    private List<SpentEnology> spentEnologies;
    private List<SpentPackaging> spentPackagings;
    private List<SulfurMeasurement> sulfurMeasurements;
    private List<Fermentation> fermentations;
    private List<GrapePress> grapePresses;
    private List<Transfusion> transfusions;

    public ReportItemGenerator(StorageSpentService storageSpentService) {
        this.storageSpentService = storageSpentService;
    }

    public SpentMaterial spentMaterialsById(long id) {
        return spentMaterials.stream().filter(spentMaterial -> spentMaterial.getId() == id).findFirst().get();
    }

    public SpentEnology spentEnologyById(long id) {
        return spentEnologies.stream().filter(spentEnology -> spentEnology.getId() == id).findFirst().get();
    }

    public SpentPackaging spentPackagingById(long id) {
        return spentPackagings.stream().filter(spentPackaging -> spentPackaging.getId() == id).findFirst().get();
    }

    public Fermentation fermentation(long id) {
        return fermentations.stream().filter(fermentation -> fermentation.getId() == id).findFirst().get();
    }

    public GrapePress pressing(long id) {
        return grapePresses.stream().filter(grapePress -> grapePress.getId() == id).findFirst().get();
    }

    public Transfusion transfusion(long id) {
        return transfusions.stream().filter(transfusion -> transfusion.getId() == id).findFirst().get();
    }

    public SulfurMeasurement sulfurMeasurement(long operationId) {
        return sulfurMeasurements.stream().filter(sulfurMeasurement -> sulfurMeasurement.getId() == operationId).findFirst().get();
    }

    public List<ReportItem> generateReportItems(Lot lot) {

        List<ReportItem> reportItems = new ArrayList<>();

        spentEnologies = storageSpentService.findSpentEnologies(lot.getId());
        spentEnologies.forEach(spentEnology -> reportItems.add(
                new ReportItem(Operations.ADD_ENOLOGY,
                        spentEnology.getId(),
                        spentEnology.getDate())));
        spentMaterials = storageSpentService.findSpentMaterials(lot.getId());
        spentMaterials.forEach(spentMaterial -> reportItems.add(
                new ReportItem(Operations.ADD_MATERIAL,
                        spentMaterial.getId(),
                        spentMaterial.getDate())));
        spentPackagings = storageSpentService.findSpentPackagings(lot.getId());
        spentPackagings.forEach(spentPackaging -> reportItems.add(
                new ReportItem(Operations.ADD_PACKAGING,
                        spentPackaging.getId(),
                        spentPackaging.getDate())));

        sulfurMeasurements = storageSpentService.getSulfurMeasurements(lot.getId());
        sulfurMeasurements.forEach(sulfurMeasurement -> reportItems.add(
                new ReportItem(Operations.SULFUR_MEASUREMENT,
                        sulfurMeasurement.getId(),
                        sulfurMeasurement.getDate())));
        fermentations = storageSpentService.getFermentations(lot.getId());
        fermentations.forEach(fermentation -> reportItems.add(
                new ReportItem(Operations.FERMENTATION,
                        fermentation.getId(),
                        fermentation.getDate())));
        grapePresses = storageSpentService.getGrapePress(lot.getId());
        grapePresses.forEach(grapePress -> reportItems.add(
                new ReportItem(Operations.PRESSING,
                        grapePress.getId(),
                        grapePress.getDate())));
        transfusions = storageSpentService.getTransfusions(lot.getId());
        transfusions.forEach(transfusion -> reportItems.add(
                new ReportItem(Operations.TRANSFUSION,
                        transfusion.getId(),
                        transfusion.getDate())));

        Collections.sort(reportItems, Comparator.comparing(obj -> obj.getDate()));
        return reportItems;
    }

}
