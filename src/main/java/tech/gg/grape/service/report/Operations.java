package tech.gg.grape.service.report;

public enum Operations {
    OPEN_LOT,
    ADD_ENOLOGY,
    ADD_MATERIAL,
    ADD_PACKAGING,
    PRESSING,
    FERMENTATION,
    SULFUR_MEASUREMENT,
    TRANSFUSION,
    END_LOT

}
