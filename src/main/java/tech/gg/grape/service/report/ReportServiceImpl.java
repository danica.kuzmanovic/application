package tech.gg.grape.service.report;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.gg.grape.service.service.LotService;
import tech.gg.grape.service.service.StorageSpentService;
import tech.gg.grape.repository.*;
import tech.gg.grape.repository.model.*;

import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    LotService lotService;

    @Autowired
    StorageSpentService storageSpentService;

    @Autowired
    SulfurFermentationRepository sulfurFermentationRepository;

    @Autowired
    FermentationRepository fermentationRepository;

    @Autowired
    BottleRepository bottleRepository;

    @Autowired
    TankRepository tankRepository;

    @Autowired
    EndLotRepository endLotRepository;

    private ReportItemGenerator reportItemGenerator;

    @Override
    public String createDocument(long lotId) throws IOException {

        File file = new File("report/");
        if (!file.exists()) {
            FileUtils.forceMkdir(file);
        }

        reportItemGenerator = new ReportItemGenerator(storageSpentService);
        String filename = "report/" + lotId + ".pdf";
        PdfWriter pdf = new PdfWriter(filename);
        PdfDocument document = new PdfDocument(pdf);
        Document doc = new Document(document, PageSize.A3);
        Lot lot = lotService.findOne(lotId);
        String title = "LOT " + lot.getLotName() + " | Tank " + lot.getTank().getOrderNumber() + " | "
                + lot.getUsedCapacity() + "L / " + lot.getTank().getCapacity() + "L";
        float columnWidth = 100F;
        float[] columnWidths = {columnWidth, columnWidth, columnWidth, columnWidth, columnWidth, columnWidth, columnWidth};
        Table table = new Table(columnWidths);
        createTitleCell(table, title);
        table.setHorizontalAlignment(HorizontalAlignment.CENTER);
        createLotTable(table, lot);
        createOperationsTable(table, lot);
        doc.add(table);
        doc.close();
        return filename;
    }

    public Table createLotTable(Table table, Lot lot) {

        createCellHeader(table, "Etiketa");
        createCell(table, lot.getWineLabel());
        createCellHeader(table, "Zapoceto");
        createCell(table, lot.getStartTime().toString());
        createCellHeader(table, "Ocekivani zavrsetak");
        createCell(table, lot.getExpectedEndDate().format(DateTimeFormatter.ofPattern("dd.MM.yy")));
        createCell(table, "");

        Optional<SulfurMeasurement> optionalSulfurMeasurement = sulfurFermentationRepository
                .getSulfurMeasurementByLotId(lot.getId()).stream().findFirst();
        Optional<Fermentation> optionalFermentation = fermentationRepository.getFermentationByLotId(lot.getId()).stream().findFirst();

        if (optionalSulfurMeasurement.isPresent() && optionalFermentation.isPresent()) {
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");

            SulfurMeasurement sulfurMeasurement = optionalSulfurMeasurement.get();
            Fermentation fermentation = optionalFermentation.get();
            createCellHeader(table, "TSO2");
            createCellHeader(table, "FSO2");
            createCellHeader(table, "Ethanol");
            createCellHeader(table, "Zaostali seceri");
            createCellHeader(table, "pH");
            createCellHeader(table, "TA");
            createCell(table, "");

            createCell(table, String.valueOf(sulfurMeasurement.getTso2()) + " mg/l");
            createCell(table, String.valueOf(sulfurMeasurement.getFso2()) + " mg/l");
            createCell(table, String.valueOf(fermentation.getAlcohol()));
            createCell(table, String.valueOf(fermentation.getSugar()) + " g/l");
            createCell(table, String.valueOf(fermentation.getpH()));
            createCell(table, String.valueOf(fermentation.getAcid()) + " g/l");
            createCell(table, "");
        }
        if (optionalSulfurMeasurement.isPresent() && optionalFermentation.isEmpty()) {

            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");

            SulfurMeasurement sulfurMeasurement = optionalSulfurMeasurement.get();
            createCellHeader(table, "TSO2");
            createCellHeader(table, "Ethanol");
            createCellHeader(table, "Zaostali seceri");
            createCellHeader(table, "FSO2");
            createCellHeader(table, "pH");
            createCellHeader(table, "TA");
            createCell(table, "");

            createCell(table, String.valueOf(sulfurMeasurement.getFso2()) + " mg/l");
            createCell(table, String.valueOf(sulfurMeasurement.getTso2()) + " mg/l");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
        }
        if (optionalSulfurMeasurement.isEmpty() && optionalFermentation.isPresent()) {

            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");
            createCell(table, "");

            Fermentation fermentation = optionalFermentation.get();
            createCellHeader(table, "TSO2");
            createCellHeader(table, "Ethanol");
            createCellHeader(table, "Zaostali seceri");
            createCellHeader(table, "FSO2");
            createCellHeader(table, "pH");
            createCellHeader(table, "TA");
            createCell(table, "");

            createCell(table, "");
            createCell(table, "");
            createCell(table, String.valueOf(fermentation.getAlcohol()));
            createCell(table, fermentation.getSugar() + " g/l");
            createCell(table, String.valueOf(fermentation.getpH()));
            createCell(table, fermentation.getAcid() + " g/l");
            createCell(table, "");
        }

        createCell(table, "");
        createCell(table, "");
        createCell(table, "");
        createCell(table, "");
        createCell(table, "");
        createCell(table, "");
        createCell(table, "");

        return table;
    }

    private void createOperationsTable(Table operationsTable, Lot lot) {

        Cell operations = new Cell(1, 7);
        PdfFont fontBold = createFontBold();
        operations.setFont(fontBold);
        operations.add("OPERACIJE").setTextAlignment(TextAlignment.CENTER);
        operationsTable.addCell(operations);

        createOpenLotRow(operationsTable, lot);

        List<ReportItem> reportItems = reportItemGenerator.generateReportItems(lot);
        reportItems.forEach(reportItem -> {
            if (reportItem.getOperation() == Operations.ADD_MATERIAL) {
                createMaterialRow(operationsTable, lot, reportItem);
            }
            if (reportItem.getOperation() == Operations.ADD_ENOLOGY) {
                createEnologyRow(operationsTable, reportItem);
            }
            if (reportItem.getOperation() == Operations.ADD_PACKAGING) {
                createPackagingRow(operationsTable, reportItem);
            }
            if (reportItem.getOperation() == Operations.FERMENTATION) {
                createFermentationRow(operationsTable, reportItem);
            }
            if (reportItem.getOperation() == Operations.PRESSING) {
                createPressingRow(operationsTable, reportItem);
            }
            if (reportItem.getOperation() == Operations.TRANSFUSION) {
                createTransfusionRow(operationsTable, reportItem);
            }
            if (reportItem.getOperation() == Operations.SULFUR_MEASUREMENT) {
                createSulfurMeasurementRow(operationsTable, reportItem);
            }
        });
        //createCloseLotRow(operationsTable, lot);
    }

    private void createCloseLotRow(Table operationsTable, Lot lot) {
        createCellHeader(operationsTable, "Zatvaranje LOT-a");
        createCellHeader(operationsTable, "LOT");
        createCellHeader(operationsTable, "Datum");
        createCellHeader(operationsTable, "Komentar");
        createCellHeader(operationsTable, "");
        createCellHeader(operationsTable, "");
        createCellHeader(operationsTable, "");

        EndLot endLot = endLotRepository.findByLotId(lot.getId());
        if (endLot != null) {
            createCell(operationsTable, "");
            createCell(operationsTable, endLot.getLot().getLotName());
            createCell(operationsTable, endLot.getDate().toString());
            createCell(operationsTable, endLot.getComment());
            createCell(operationsTable, "");
            createCell(operationsTable, "");
            createCell(operationsTable, "");
        } else {
            createCell(operationsTable, "");
            createCell(operationsTable, "");
            createCell(operationsTable, "");
            createCell(operationsTable, "");
            createCell(operationsTable, "");
            createCell(operationsTable, "");
            createCell(operationsTable, "");
        }

    }

    private void createSulfurMeasurementRow(Table operationsTable, ReportItem reportItem) {
        createCellHeader(operationsTable, "Merenje sumpora");
        createCellHeader(operationsTable, "Datum");
        createCellHeader(operationsTable, "FSO2");
        createCellHeader(operationsTable, "TSO2");
        createCellHeader(operationsTable, "");
        createCellHeader(operationsTable, "");
        createCellHeader(operationsTable, "");


        SulfurMeasurement sulfurMeasurement = reportItemGenerator.sulfurMeasurement(reportItem.getOperationId());
        createCell(operationsTable, "");
        createCell(operationsTable, sulfurMeasurement.getDate().toString());
        createCell(operationsTable, String.valueOf(sulfurMeasurement.getFso2()) + "mg/L");
        createCell(operationsTable, String.valueOf(sulfurMeasurement.getTso2()) + "mg/L");
        createCell(operationsTable, "");
        createCell(operationsTable, "");
        createCell(operationsTable, "");
    }

    private void createTransfusionRow(Table operationsTable, ReportItem reportItem) {
        createCellHeader(operationsTable, "Pretakanje");
        createCellHeader(operationsTable, "Datum");
        createCellHeader(operationsTable, "Pretakanje u tank");
        createCellHeader(operationsTable, "Litara");
        createCellHeader(operationsTable, "Otpad");
        createCellHeader(operationsTable, "Komentar");
        createCellHeader(operationsTable, "");


        Transfusion transfusion = reportItemGenerator.transfusion(reportItem.getOperationId());
        createCell(operationsTable, "");
        createCell(operationsTable, transfusion.getDate().toString());
        Tank tank = tankRepository.findById(transfusion.getTargetTankId());
        createCell(operationsTable, String.valueOf(tank.getOrderNumber()));
        createCell(operationsTable, transfusion.getWineAmount() + "L");
        createCell(operationsTable, transfusion.getWasteAmount() + "L");
        createCell(operationsTable, transfusion.getComment());
        createCell(operationsTable, "");
    }

    private void createPressingRow(Table operationsTable, ReportItem reportItem) {
        createCellHeader(operationsTable, "Presovanje");
        createCellHeader(operationsTable, "Datum");
        createCellHeader(operationsTable, "Litara");
        createCellHeader(operationsTable, "Komentar");
        createCellHeader(operationsTable, "");
        createCellHeader(operationsTable, "");
        createCellHeader(operationsTable, "");


        GrapePress grapePress = reportItemGenerator.pressing(reportItem.getOperationId());
        createCell(operationsTable, "");
        createCell(operationsTable, grapePress.getDate().toString());
        createCell(operationsTable, grapePress.getQuantity() + "L");
        createCell(operationsTable, String.valueOf(grapePress.getComment()));
        createCell(operationsTable, "");
        createCell(operationsTable, "");
        createCell(operationsTable, "");
    }

    private void createFermentationRow(Table operationsTable, ReportItem reportItem) {

        createCellHeader(operationsTable, "Merenje");
        createCellHeader(operationsTable, "Datum");
        createCellHeader(operationsTable, "Ethanol");
        createCellHeader(operationsTable, "Zaostali seceri");
        createCellHeader(operationsTable, "pH");
        createCellHeader(operationsTable, "TA");
        createCellHeader(operationsTable, "Temperatura");

        Fermentation fermentation = reportItemGenerator.fermentation(reportItem.getOperationId());
        createCell(operationsTable, "");
        createCell(operationsTable, fermentation.getDate().toString());
        if (fermentation.getAlcohol() == null) {
            createCell(operationsTable, "");
        } else {
            createCellHeader(operationsTable, String.valueOf(fermentation.getAlcohol()));
        }
        if (fermentation.getSugar() == null) {
            createCell(operationsTable, "");
        } else {
            createCellHeader(operationsTable, String.valueOf(fermentation.getSugar()));
        }
        if (fermentation.getpH() == null) {
            createCell(operationsTable, "");
        } else {
            createCell(operationsTable, String.valueOf(fermentation.getpH()));
        }
        if (fermentation.getAcid() == null) {
            createCell(operationsTable, "");
        } else {
            createCell(operationsTable, fermentation.getAcid() + "g/L");
        }
        if (fermentation.getTemperature() == null) {
            createCell(operationsTable, "");
        } else {
            createCell(operationsTable, String.valueOf(fermentation.getTemperature()));
        }
    }

    private void createPackagingRow(Table operationsTable, ReportItem reportItem) {

        SpentPackaging spentPackaging = reportItemGenerator.spentPackagingById(reportItem.getOperationId());
        Bottle bottle = null;
        if (spentPackaging.getPackaging().getCategory().ordinal() == 0) {
            bottle = bottleRepository.findByPackagingId(spentPackaging.getId());
        }

        createCellHeader(operationsTable, "Flasiranje");
        createCellHeader(operationsTable, "Datum");
        createCellHeader(operationsTable, "Kategorija");
        createCellHeader(operationsTable, "Oznaka");
        if (bottle != null) {
            createCellHeader(operationsTable, "Zapremina");
        }
        createCellHeader(operationsTable, "Kolicina");
        createCellHeader(operationsTable, "");
        if (bottle == null) {
            createCellHeader(operationsTable, "");
        }

        createCell(operationsTable, "");
        createCell(operationsTable, spentPackaging.getDate().toString());
        createCell(operationsTable, PackagingCategoryConverter.convert(spentPackaging.getPackaging().getCategory()));
        createCell(operationsTable, spentPackaging.getPackaging().getName());
        if (bottle != null) {
            createCell(operationsTable, String.valueOf(bottle.getVolume()) + "mL");
        }
        createCell(operationsTable, String.valueOf(spentPackaging.getQuantity()) + "kom");
        if (bottle == null) {
            createCell(operationsTable, "");
        }
        createCell(operationsTable, "");
    }


    private void createEnologyRow(Table operationsTable, ReportItem reportItem) {

        createCellHeader(operationsTable, "Dodatak pomocnih sredstava");
        createCellHeader(operationsTable, "Datum");
        createCellHeader(operationsTable, "Kategorija");
        createCellHeader(operationsTable, "Oznaka");
        createCellHeader(operationsTable, "Kolicina");
        createCellHeader(operationsTable, "Komentar");
        createCellHeader(operationsTable, "");

        SpentEnology spentEnology = reportItemGenerator.spentEnologyById(reportItem.getOperationId());
        createCell(operationsTable, "");
        createCell(operationsTable, spentEnology.getDate().toString());
        createCell(operationsTable, spentEnology.getEnology().getCategory().getName());
        createCell(operationsTable, spentEnology.getEnology().getName());
        createCell(operationsTable, spentEnology.getQuantity() + "g");
        createCell(operationsTable, spentEnology.getComment());
        createCell(operationsTable, "");
    }

    private void createMaterialRow(Table operationsTable, Lot lot, ReportItem reportItem) {

        createCellHeader(operationsTable, "Prijem sirovina");
        createCellHeader(operationsTable, "Datum");
        createCellHeader(operationsTable, "Osnovna sirovina");
        createCellHeader(operationsTable, "Kolicina");
        createCellHeader(operationsTable, "");
        createCellHeader(operationsTable, "");
        createCellHeader(operationsTable, "");

        SpentMaterial spentMaterial = reportItemGenerator.spentMaterialsById(reportItem.getOperationId());
        createCell(operationsTable, "");
        createCell(operationsTable, spentMaterial.getDate().toString());
        createCell(operationsTable, spentMaterial.getRawMaterial().getName());
        createCell(operationsTable, String.valueOf(spentMaterial.getQuantity()) + "kg");
        createCell(operationsTable, "");
        createCell(operationsTable, "");
        createCell(operationsTable, "");
    }

    private void createOpenLotRow(Table operationsTable, Lot lot) {
        createCellHeader(operationsTable, "Otvaranje LOT-a");
        createCellHeader(operationsTable, "LOT");
        createCellHeader(operationsTable, "Datum pocetka");
        createCellHeader(operationsTable, "Ocekivani zavrsetak");
        createCellHeader(operationsTable, "Naziv etikete");
        createCellHeader(operationsTable, "");
        createCellHeader(operationsTable, "");

        createCell(operationsTable, "");
        createCell(operationsTable, lot.getLotName());
        createCell(operationsTable, lot.getStartTime().toString());
        createCell(operationsTable, lot.getExpectedEndDate().toString());
        createCell(operationsTable, lot.getWineLabel());
        createCell(operationsTable, "");
        createCell(operationsTable, "");
    }

    private void createTitleCell(Table table, String content) {

        Cell cell = new Cell(1, 7);
        PdfFont font = createFontBold();
        cell.setBorder(Border.NO_BORDER);
        cell.setMinHeight(14);
        cell.add(content).setFont(font).setFontSize(30);
        table.addCell(cell);
    }

    private void createCell(Table table, String content) {
        Cell cell = new Cell();
        PdfFont font = createFont();
        cell.setMinHeight(14);
        cell.add(content).setFont(font).setFontSize(10);
        table.addCell(cell);
    }

    private PdfFont createFont() {
        PdfFont pdfFont = null;
        try {
            pdfFont = PdfFontFactory.createFont(FontConstants.HELVETICA);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pdfFont;
    }

    private PdfFont createFontBold() {
        PdfFont font = null;
        try {
            font = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return font;
    }

    private void createCellHeader(Table table, String content) {
        Cell cell = new Cell();
        cell.setMinHeight(14);
        PdfFont font = createFontBold();
        cell.add(content).setFont(font).setFontSize(10);
        table.addCell(cell);
    }
}
