package tech.gg.grape.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.gg.grape.service.service.EnologyService;
import tech.gg.grape.repository.EnologyCategoryRepository;
import tech.gg.grape.repository.EnologyRepository;
import tech.gg.grape.repository.MoneyRepository;
import tech.gg.grape.repository.model.Enology;
import tech.gg.grape.repository.model.EnologyCategory;

import java.util.List;

@Service
public class EnologyServiceImpl implements EnologyService {

    @Autowired
    EnologyRepository enologyRepository;

    @Autowired
    MoneyRepository moneyRepository;

    @Autowired
    EnologyCategoryRepository enologyCategoryRepository;

    @Override
    public Enology findOne(long id) {
        return enologyRepository.findOneById(id);
    }

    @Override
    public Enology save(Enology enology) {
        moneyRepository.save(enology.getMoney());
        return enologyRepository.save(enology);
    }

    @Override
    public Enology changeQuantity(Enology enology, double quantity) {
        Enology updated = new Enology(enology.getId(), enology.getName(), enology.getQuantity() + quantity,
                enology.getCategory(), enology.getMoney());
        return enologyRepository.save(updated);
    }

    @Override
    public void delete(Enology enology) {
        enology.setDeleted(true);
        enologyRepository.save(enology);
    }

    @Override
    public List<Enology> findAll() {
        return enologyRepository.findAllByDeletedFalse();
    }

    @Override
    public Enology findByNameAndCategoryAndAmount(String name, int category, double amount) {
        return enologyRepository.findByNameAndCategoryAndAmount(name, category, amount);
    }

    @Override
    public List<EnologyCategory> findAllCategories() {
        return enologyCategoryRepository.findAll() ;
    }

    @Override
    public void saveCategory(EnologyCategory category) {
        enologyCategoryRepository.save(category);
    }

    @Override
    public EnologyCategory findCategory(int category) {
        return enologyCategoryRepository.findById(category);
    }

}
