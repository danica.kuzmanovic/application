package tech.gg.grape.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.gg.grape.service.service.TransfusionService;
import tech.gg.grape.repository.TransfusionRepository;
import tech.gg.grape.repository.model.Transfusion;

@Service
public class TransfusionServiceImpl implements TransfusionService {

    @Autowired
    TransfusionRepository transfusionRepository;

    @Override
    public void save(Transfusion transfusion) {
        transfusionRepository.save(transfusion);
    }
}
