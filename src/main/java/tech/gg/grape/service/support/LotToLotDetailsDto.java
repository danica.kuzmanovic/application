package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.LotDetailsDto;
import tech.gg.grape.repository.model.Lot;
import tech.gg.grape.repository.model.Tank;
import tech.gg.grape.service.service.TankService;

import java.util.ArrayList;
import java.util.List;

@Component
public class LotToLotDetailsDto {

    @Autowired
    TankService tankService;

    LotDetailsDto lotDetailsDto =null;

    public LotDetailsDto convert(Lot lot) {
        if (lot.getTank() == null){
            return null;
        }
        Tank tank = tankService.findOneById(lot.getTank().getId());
        if (tank == null) {
            return null;
        }
        lotDetailsDto = new LotDetailsDto((int) lot.getId(), lot.getLotName(), lot.getWineLabel(),
                lot.getWineColor().ordinal(), lot.getStartTime(),
                lot.getExpectedEndDate(),
                tank.getOrderNumber(),
                (int) lot.getUsedCapacity(),
                tank.getCapacity());
        return lotDetailsDto;
    }

    public List<LotDetailsDto> convert(List<Lot> lots) {
        List<LotDetailsDto> lotDetailsDto = new ArrayList<>();
        for(Lot lot: lots){
            LotDetailsDto lotDetailsDto1 = convert(lot);
            if (lotDetailsDto1 == null) {
                return null;
            }
            lotDetailsDto.add(lotDetailsDto1);
        }
        return lotDetailsDto;
    }
}
