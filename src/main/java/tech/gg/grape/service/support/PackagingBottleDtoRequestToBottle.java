package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.PackagingBottleDtoRequest;
import tech.gg.grape.service.service.PackagingService;
import tech.gg.grape.repository.model.Bottle;
import tech.gg.grape.repository.model.Packaging;
import tech.gg.grape.repository.model.VolumeUnit;

@Component
public class PackagingBottleDtoRequestToBottle {

    @Autowired
    PackagingService packagingService;

    public Bottle convert(PackagingBottleDtoRequest source, Packaging packaging) {
        return new Bottle(source.getVolume(), VolumeUnit.values()[source.getVolumeUnit()],
                packagingService.findOne(packaging.getId()));
    }
}
