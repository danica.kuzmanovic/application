package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.EndLotDto;
import tech.gg.grape.service.service.LotService;
import tech.gg.grape.repository.model.EndLot;
import tech.gg.grape.repository.model.Lot;

@Component
public class EndLotDtoToEndLot {

    @Autowired
    LotService lotService;

    public EndLot convert(EndLotDto source) {
        Lot lot = lotService.findOne(source.getLotId());
        if (lot == null) {
            return null;
        }
        return new EndLot(lot, source.getDate(), source.getComment());
    }
}
