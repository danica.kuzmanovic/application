package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.TransfusionDto;
import tech.gg.grape.service.service.LotService;
import tech.gg.grape.repository.model.Lot;
import tech.gg.grape.repository.model.Transfusion;

@Component
public class TransfusionDtoToTransfusion implements Converter<TransfusionDto, Transfusion> {

    @Autowired
    LotService lotService;

    @Override
    public Transfusion convert(TransfusionDto transfusionDto) {
        Lot lot = lotService.findOneById(transfusionDto.getLotId());
        return new Transfusion(lot.getTank().getId(), transfusionDto.getTargetTankId(), transfusionDto.getLotId(),
                transfusionDto.getWineAmount(), transfusionDto.getWasteAmount(), transfusionDto.getDate(),
                transfusionDto.getComment());
    }
}
