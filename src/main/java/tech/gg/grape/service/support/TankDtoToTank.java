package tech.gg.grape.service.support;

import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.TankDtoRequest;
import tech.gg.grape.repository.model.Tank;
import tech.gg.grape.repository.model.TankType;

@Component
public class TankDtoToTank {

    public Tank convert(TankDtoRequest source) {
        return new Tank(source.getOrderNumber(), source.getCapacity(), TankType.TANK.values()[source.getTankType()]);
    }
}
