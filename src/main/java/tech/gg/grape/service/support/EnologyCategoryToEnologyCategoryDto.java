package tech.gg.grape.service.support;

import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.EnologyCategoryDto;
import tech.gg.grape.repository.model.EnologyCategory;

import java.util.ArrayList;
import java.util.List;

@Component
public class EnologyCategoryToEnologyCategoryDto {

    public EnologyCategoryDto convert(EnologyCategory category) {
        return new EnologyCategoryDto(category.getId(), category.getName());
    }

    public List<EnologyCategoryDto> convert(List<EnologyCategory> categories) {
        List<EnologyCategoryDto> categoryDtos = new ArrayList<>();
        for (EnologyCategory category : categories) {
            categoryDtos.add(convert(category));
        }
        return categoryDtos;
    }
}
