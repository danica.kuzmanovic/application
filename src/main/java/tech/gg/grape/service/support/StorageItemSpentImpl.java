package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.gg.grape.service.service.StorageSpentService;
import tech.gg.grape.repository.*;
import tech.gg.grape.repository.model.*;

import java.util.List;

@Service
public class StorageItemSpentImpl implements StorageSpentService {

    @Autowired
    SpentMaterialRepository spentMaterialRepository;

    @Autowired
    SpentEnologyRepository spentEnologyRepository;

    @Autowired
    GrapePressRepository grapePressRepository;

    @Autowired
    FermentationRepository fermentationRepository;

    @Autowired
    SulfurFermentationRepository sulfurFermentationRepository;

    @Autowired
    SpentPackagingRepository spentPackagingRepository;

    @Autowired
    TransfusionRepository transfusionRepository;

    @Override
    public void save(SpentMaterial spentMaterial) {
        spentMaterialRepository.save(spentMaterial);
    }

    @Override
    public void save(SpentEnology spentEnology) {
        spentEnologyRepository.save(spentEnology);
    }

    @Override
    public void save(GrapePress grapePress) {
        grapePressRepository.save(grapePress);
    }

    @Override
    public void save(Fermentation fermentation) {
        fermentationRepository.save(fermentation);
    }

    @Override
    public void save(SulfurMeasurement sulfurMeasurement) {
        sulfurFermentationRepository.save(sulfurMeasurement);
    }

    @Override
    public void save(SpentPackaging spentPackaging) {
        spentPackagingRepository.save(spentPackaging);
    }

    @Override
    public List<SulfurMeasurement> getSulfurForLotId(int lotId) {
        return sulfurFermentationRepository.getSulfurMeasurementByLotId(lotId);
    }

    @Override
    public List<Fermentation> getMeasurementsForLotId(int lotId) {
        return fermentationRepository.findAllByLotId(lotId);
    }

    @Override
    public List<SpentEnology> findSpentEnologies(long id) {
        return spentEnologyRepository.findAllByLotId(id);
    }

    @Override
    public List<SpentMaterial> findSpentMaterials(long id) {
        return spentMaterialRepository.findByLotId(id);
    }

    @Override
    public List<SpentPackaging> findSpentPackagings(long id) {
        return spentPackagingRepository.findByLotId(id);
    }

    @Override
    public List<SulfurMeasurement> getSulfurMeasurements(long id) {
        return sulfurFermentationRepository.getSulfurMeasurementByLotId(id);
    }

    @Override
    public List<Fermentation> getFermentations(long id) {
        return fermentationRepository.findAllByLotId(id);
    }

    @Override
    public List<GrapePress> getGrapePress(long id) {
        return grapePressRepository.findAllByLotId(id);
    }

    @Override
    public List<Transfusion> getTransfusions(long id) {
        return transfusionRepository.findAllByLotId(id);
    }

    @Override
    public Fermentation findFermentationdById(long id) {
        return fermentationRepository.findById(id);
    }

    @Override
    public List<Fermentation> findFermentationdsByLotId(long id) {
        return fermentationRepository.findAllByLotId(id);
    }
}
