package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.PackagingDto;
import tech.gg.grape.service.service.PackagingService;
import tech.gg.grape.repository.model.Packaging;

import java.util.ArrayList;
import java.util.List;

@Component
public class PackagingToPackagingDtoResponse {

    @Autowired
    PackagingService packagingService;

    public PackagingDto convert(Packaging source) {
        return new PackagingDto(source.getId(), source.getCategory().ordinal(), source.getName(),
                source.getQuantity(), source.getMoney().getAmount(), source.getMoney().getUnit().ordinal());
    }

    public List<PackagingDto> convert(List<Packaging> packagings) {
        List<PackagingDto> packagingDtos = new ArrayList<>();
        for (Packaging packaging : packagings) {
            packagingDtos.add(convert(packaging));
        }
        return packagingDtos;
    }
}
