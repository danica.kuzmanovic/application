package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.EnologyDtoRequest;
import tech.gg.grape.repository.EnologyCategoryRepository;
import tech.gg.grape.repository.MoneyRepository;
import tech.gg.grape.repository.model.*;

import java.util.Optional;

@Component
public class EnologyDtoToEnology {

    @Autowired
    MoneyRepository moneyRepository;

    @Autowired
    EnologyCategoryRepository categoryRepository;

    public Enology convert(EnologyDtoRequest source) {

        Optional<Money> money = moneyRepository.findByAmountAndUnit(source.getAmount(), source.getUnit());
        EnologyCategory category = categoryRepository.findById(source.getCategory());
        if (money.isEmpty()) {
            Money newMoney = new Money(source.getAmount(), Unit.values()[source.getUnit()]);
            moneyRepository.save(newMoney);
            return new Enology(source.getName(), source.getQuantity(), category,
                    newMoney);
        }
        return new Enology(source.getName(), source.getQuantity(), category,
                money.get());
    }
}
