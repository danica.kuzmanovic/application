package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.RawMaterialDtoRequest;
import tech.gg.grape.repository.MoneyRepository;
import tech.gg.grape.repository.model.*;

import java.util.Optional;

@Component
public class RawMaterialDtoToRawMaterial {

    @Autowired
    MoneyRepository moneyRepository;

    public RawMaterial convert(RawMaterialDtoRequest source) {

        Optional<Money> money = moneyRepository.findByAmountAndUnit(source.getAmount(), source.getUnit());
        if (money.isEmpty()) {
            Money newMoney = new Money(source.getAmount(), Unit.values()[source.getUnit()]);
            moneyRepository.save(newMoney);
            return new RawMaterial(source.getName(), source.getQuantity(), newMoney);
        }
        return new RawMaterial(source.getName(), source.getQuantity(), money.get());
    }
}
