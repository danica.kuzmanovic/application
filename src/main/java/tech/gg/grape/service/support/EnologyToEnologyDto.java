package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.EnologyDtoResponse;
import tech.gg.grape.service.service.EnologyService;
import tech.gg.grape.repository.model.Enology;
import tech.gg.grape.repository.model.EnologyCategory;

import java.util.ArrayList;
import java.util.List;

@Component
public class EnologyToEnologyDto implements Converter<Enology, EnologyDtoResponse> {

    @Autowired
    EnologyService enologyService;

    @Override
    public EnologyDtoResponse convert(Enology enology) {
        EnologyCategory category = enologyService.findCategory(enology.getCategory().getId());
        return new EnologyDtoResponse(enology.getId(), enology.getName(), enology.getQuantity(),
                enology.getCategory().getName(), enology.getMoney().getAmount(), enology.getMoney().getUnit().ordinal());
    }

    public List<EnologyDtoResponse> convert(List<Enology> enologies) {
        List<EnologyDtoResponse> enologyDtos = new ArrayList<>();
        for (Enology enology : enologies) {
            enologyDtos.add(convert(enology));
        }
        return enologyDtos;
    }
}
