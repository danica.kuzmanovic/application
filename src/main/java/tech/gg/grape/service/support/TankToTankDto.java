package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.TankDetailsDto;
import tech.gg.grape.controller.dto.TankDtoResponse;
import tech.gg.grape.service.service.LotService;
import tech.gg.grape.repository.model.Lot;
import tech.gg.grape.repository.model.Tank;

import java.util.ArrayList;
import java.util.List;

@Component
public class TankToTankDto {

    @Autowired
    LotService lotService;

    public TankDtoResponse convert(Tank source) {
        Lot lot = lotService.findOneByTankId(source.getId());
        if (lot != null) {
            TankDetailsDto tankDetailsDto = new TankDetailsDto(lot.getLotName(), (int) lot.getUsedCapacity(),
                    lot.getWineColor().ordinal());
            return new TankDtoResponse(source.getId(), source.getOrderNumber(), source.getCapacity(),
                    source.getTankType().ordinal(), tankDetailsDto);
        }
        return new TankDtoResponse(source.getId(), source.getOrderNumber(), source.getCapacity(),
                source.getTankType().ordinal(), null);
    }

    public List<TankDtoResponse> convert(List<Tank> source) {
        List<TankDtoResponse> tankDtos = new ArrayList<>();
        for (Tank tank : source) {
            tankDtos.add(convert(tank));
        }
        return tankDtos;
    }
}
