package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.PackagingBottleDtoRequest;
import tech.gg.grape.repository.MoneyRepository;
import tech.gg.grape.repository.model.*;

import java.util.Optional;

@Component
public class PackagingBottleDtoToPackaging {

    @Autowired
    MoneyRepository moneyRepository;

    public Packaging convert(PackagingBottleDtoRequest source) {

        Optional<Money> money = moneyRepository.findByAmountAndUnit(source.getAmount(), source.getUnit());
        if (money.isEmpty()) {
            Money newMoney = new Money(source.getAmount(), Unit.values()[source.getUnit()]);
            moneyRepository.save(newMoney);
            return new Packaging(PackagingCategory.values()[source.getCategory()], source.getName(),
                    source.getQuantity(), newMoney);
        }
        return new Packaging(PackagingCategory.values()[source.getCategory()], source.getName(),
                source.getQuantity(), money.get());
    }
}
