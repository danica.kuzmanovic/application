package tech.gg.grape.service.support;

import tech.gg.grape.repository.model.SpentEnology;
import tech.gg.grape.service.report.Operations;
import tech.gg.grape.service.report.ReportItem;

import java.util.ArrayList;
import java.util.List;

public class SpentEnologyToReportItem {

    public ReportItem convert(SpentEnology spentEnology) {
        return new ReportItem(Operations.ADD_ENOLOGY, spentEnology.getId(), spentEnology.getDate());
    }

    public List<ReportItem> convert(List<SpentEnology> spentEnologies) {
        List<ReportItem> reportItems = new ArrayList<>();
        for (SpentEnology spentEnology : spentEnologies) {
            reportItems.add(convert(spentEnology));
        }
        return reportItems;
    }
}
