package tech.gg.grape.service.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.CreateLotDto;
import tech.gg.grape.service.service.LotService;
import tech.gg.grape.service.service.PackagingService;
import tech.gg.grape.service.service.TankService;
import tech.gg.grape.repository.model.Lot;
import tech.gg.grape.repository.model.Tank;
import tech.gg.grape.repository.model.WineColor;

@Component
public class LotDtoToLot {

    @Autowired
    LotService lotService;

    @Autowired
    PackagingService packagingService;

    @Autowired
    TankService tankService;


    public Lot convert(CreateLotDto lotDto, Tank tank) {
        return new Lot(lotDto.getLotName(), lotDto.getStartDate(), lotDto.getExpectedEndDate(), lotDto.getWineLabel(),
                WineColor.values()[lotDto.getWineColor()], 0, tank);
    }
}
