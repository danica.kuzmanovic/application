package tech.gg.grape.service.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.PackagingBottleDto;
import tech.gg.grape.repository.model.Bottle;

import java.util.ArrayList;
import java.util.List;

@Component
public class PackagingBottleToPackagingBottleDto implements Converter<Bottle, PackagingBottleDto> {
    @Override
    public PackagingBottleDto convert(Bottle source) {
        return new PackagingBottleDto(source.getId(), source.getVolume(), source.getVolumeUnit().ordinal(), source.getPackaging().getId());
    }

    public List<PackagingBottleDto> convert(List<Bottle> bottles) {
        List<PackagingBottleDto> packagingBottleDtos = new ArrayList<>();
        for (Bottle bottle : bottles) {
            packagingBottleDtos.add(convert(bottle));
        }
        return packagingBottleDtos;
    }
}
