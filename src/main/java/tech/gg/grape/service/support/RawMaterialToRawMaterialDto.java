package tech.gg.grape.service.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.RawMaterialDtoResponse;
import tech.gg.grape.repository.model.RawMaterial;

import java.util.ArrayList;
import java.util.List;

@Component
public class RawMaterialToRawMaterialDto implements Converter<RawMaterial, RawMaterialDtoResponse> {

    @Override
    public RawMaterialDtoResponse convert(RawMaterial source) {
        return new RawMaterialDtoResponse(source.getId(), source.getName(), source.getQuantity(),
                source.getMoney().getAmount(), source.getMoney().getUnit().ordinal());
    }

    public List<RawMaterialDtoResponse> convert(List<RawMaterial> source) {
        List<RawMaterialDtoResponse> dtos = new ArrayList<>();
        for (RawMaterial rawMaterial : source) {
            dtos.add(convert(rawMaterial));
        }
        return dtos;
    }
}
