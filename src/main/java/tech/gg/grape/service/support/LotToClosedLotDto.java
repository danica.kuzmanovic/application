package tech.gg.grape.service.support;

import org.springframework.stereotype.Component;
import tech.gg.grape.controller.dto.ClosedLotDto;
import tech.gg.grape.repository.model.EndLot;

import java.util.ArrayList;
import java.util.List;

@Component
public class LotToClosedLotDto {

    public ClosedLotDto convert(EndLot endLot) {
        return new ClosedLotDto((int)endLot.getLot().getId(), endLot.getLot().getLotName(), endLot.getDate().toString());
    }

    public List<ClosedLotDto> convert(List<EndLot> lots) {
        List<ClosedLotDto> closedLotDtos = new ArrayList<>();
        for (EndLot lot : lots){
            closedLotDtos.add(convert(lot));
        }
        return  closedLotDtos;
    }
}
