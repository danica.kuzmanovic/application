package tech.gg.grape.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.gg.grape.service.service.PackagingService;
import tech.gg.grape.repository.BottleRepository;
import tech.gg.grape.repository.MoneyRepository;
import tech.gg.grape.repository.PackagingRepository;
import tech.gg.grape.repository.model.Bottle;
import tech.gg.grape.repository.model.Packaging;

import java.util.List;

@Service
public class PackagingServiceImpl implements PackagingService {

    @Autowired
    PackagingRepository packagingRepository;

    @Autowired
    BottleRepository bottleRepository;

    @Autowired
    MoneyRepository moneyRepository;



    @Override
    public Packaging save(Packaging packaging) {
        moneyRepository.save(packaging.getMoney());
        return packagingRepository.save(packaging);
    }

    @Override
    public Packaging changeQuantity(Packaging packaging, double quantity) {
        Packaging updated = new Packaging(packaging.getId(), packaging.getCategory(), packaging.getName(),
                packaging.getQuantity() + quantity, packaging.getMoney());
        return packagingRepository.save(updated);
    }

    @Override
    public Bottle save(Bottle bottle) {
        return bottleRepository.save(bottle);
    }


    @Override
    public void delete(Packaging packaging) {
        packaging.setDeleted(true);
        packagingRepository.save(packaging);
    }

    @Override
    public List<Packaging> findAll() {
        return packagingRepository.findByDeletedFalse();
    }

    @Override
    public Packaging findOne(long id) {
        return packagingRepository.findOneById(id);
    }

    @Override
    public Packaging findOneById(int packagingId) {
        return packagingRepository.findOneById(packagingId);
    }

    @Override
    public Bottle findBottleForPackagingId(Long packagingId) {
        return bottleRepository.findByPackagingId(packagingId);
    }

    @Override
    public Bottle findBottleById(int packagingId) {
        return bottleRepository.findByPackagingId(packagingId);
    }

    @Override
    public Packaging findByNameAndCategoryAndAmount(String name, int category, double amount) {
        return packagingRepository.findByNameAndCategoryAndAmount(name, category, amount);
    }
}
