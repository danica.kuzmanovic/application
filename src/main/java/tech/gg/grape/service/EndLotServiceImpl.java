package tech.gg.grape.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.gg.grape.service.service.EndLotService;
import tech.gg.grape.repository.EndLotRepository;
import tech.gg.grape.repository.model.EndLot;

import java.util.List;

@Service
public class EndLotServiceImpl implements EndLotService {

    @Autowired
    EndLotRepository endLotRepository;

    @Override
    public void save(EndLot endLot) {
        endLotRepository.save(endLot);
    }

    @Override
    public List<EndLot> findAll() {
        return endLotRepository.findAll();
    }
}
