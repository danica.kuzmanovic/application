package tech.gg.grape.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.gg.grape.service.service.RawMaterialService;
import tech.gg.grape.repository.MoneyRepository;
import tech.gg.grape.repository.RawMaterialRepository;
import tech.gg.grape.repository.model.RawMaterial;

import java.util.List;

@Service
public class RawMaterialServiceImpl implements RawMaterialService {

    @Autowired
    RawMaterialRepository rawMaterialRepository;

    @Autowired
    MoneyRepository moneyRepository;

    @Override
    public List<RawMaterial> findAll() {
        return rawMaterialRepository.findAllByDeletedFalse();
    }

    @Override
    public RawMaterial findOne(long id) {
        return rawMaterialRepository.findOneById(id);
    }

    @Override
    public RawMaterial changeQuantity(RawMaterial rawMaterial, double quantity) {
        RawMaterial updated = new RawMaterial(rawMaterial.getId(), rawMaterial.getName(),
                rawMaterial.getQuantity() + quantity, rawMaterial.getMoney());
        return rawMaterialRepository.save(updated);
    }

    @Override
    public RawMaterial save(RawMaterial rawMaterial) {
        moneyRepository.save(rawMaterial.getMoney());
        return rawMaterialRepository.save(rawMaterial);
    }

    @Override
    public void delete(RawMaterial rawMaterial) {
        rawMaterial.setDeleted(true);
        rawMaterialRepository.save(rawMaterial);
    }

    @Override
    public RawMaterial findByNameAndAmount(String name, double amount) {
        return rawMaterialRepository.findByNameAndAmount(name, amount);
    }
}
