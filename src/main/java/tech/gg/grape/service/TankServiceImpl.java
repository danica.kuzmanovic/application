package tech.gg.grape.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.gg.grape.service.service.TankService;
import tech.gg.grape.repository.TankRepository;
import tech.gg.grape.repository.model.Tank;

import java.util.List;

@Service
public class TankServiceImpl implements TankService {

    @Autowired
    TankRepository tankRepository;

    @Override
    public List<Tank> findAll() {
        return tankRepository.findAll();
    }

    @Override
    public Tank save(Tank tank) {
        return tankRepository.save(tank);
    }

    @Override
    public Tank findOneById(long id) {
        return tankRepository.getById(id);
    }

    @Override
    public void delete(Tank tank) {
        tankRepository.delete(tank);
    }

    @Override
    public Tank findOneByLotId(long lotId) {
        return tankRepository.findById(lotId);
    }

    @Override
    public Tank findOneByOrderNumber(long orderNumber) {
        return tankRepository.findByOrderNumber(orderNumber);
    }
}
