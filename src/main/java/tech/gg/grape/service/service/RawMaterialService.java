package tech.gg.grape.service.service;

import tech.gg.grape.repository.model.RawMaterial;

import java.util.List;

public interface RawMaterialService {

    List<RawMaterial> findAll();

    RawMaterial findOne(long id);

    RawMaterial changeQuantity(RawMaterial rawMaterial, double quantity);

    RawMaterial save(RawMaterial rawMaterial);

    void delete(RawMaterial rawMaterial);

    RawMaterial findByNameAndAmount(String name, double amount);
}
