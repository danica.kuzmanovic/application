package tech.gg.grape.service.service;

import tech.gg.grape.repository.model.EndLot;

import java.util.List;

public interface EndLotService {

    void save(EndLot endLot);

    List<EndLot> findAll();
}
