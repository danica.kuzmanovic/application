package tech.gg.grape.service.service;

import tech.gg.grape.repository.model.*;

import java.util.List;

public interface StorageSpentService {

    void save(SpentMaterial spentMaterial);

    void save(SpentEnology spentEnology);

    void save(GrapePress grapePress);

    void save(Fermentation fermentation);

    void save(SulfurMeasurement sulfurMeasurement);

    void save(SpentPackaging spentPackaging);

    List<SulfurMeasurement> getSulfurForLotId(int lotId);

    List<Fermentation> getMeasurementsForLotId(int id);

    List<SpentEnology> findSpentEnologies(long id);

    List<SpentMaterial> findSpentMaterials(long id);

    List<SpentPackaging> findSpentPackagings(long id);

    List<SulfurMeasurement> getSulfurMeasurements(long id);

    List<Fermentation> getFermentations(long id);

    List<GrapePress> getGrapePress(long id);

    List<Transfusion> getTransfusions(long id);

    Fermentation findFermentationdById(long index);

    List<Fermentation> findFermentationdsByLotId(long id);
}
