package tech.gg.grape.service.service;

import tech.gg.grape.repository.model.Transfusion;

public interface TransfusionService {

    void save(Transfusion convert);
}
