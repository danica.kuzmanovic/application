package tech.gg.grape.service.service;

import tech.gg.grape.repository.model.Tank;

import java.util.List;

public interface TankService {

    List<Tank> findAll();

    Tank save(Tank tank);

    Tank findOneById(long id);

    void delete(Tank tank);

    Tank findOneByLotId(long lotId);

    Tank findOneByOrderNumber(long orderNumber);
}
