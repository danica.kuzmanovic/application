package tech.gg.grape.service.service;

import tech.gg.grape.repository.model.Lot;
import tech.gg.grape.repository.model.SpentEnology;
import tech.gg.grape.repository.model.SpentMaterial;
import tech.gg.grape.repository.model.SpentPackaging;

import java.util.List;

public interface LotService {

    Lot findOne(long id);

    List<Lot> findAll();

    Lot findOneByTankId(long id);

    Lot save(Lot lot);

    List<SpentMaterial> findAllSpentMaterial(long lotId);

    List<SpentEnology> findAllSpentEnology(long lotId);

    List<SpentPackaging> findAllSpentPackaging(long lotId);

    Long findTankIdByUser(long id);

    Lot findOneById(long lotId);
}
