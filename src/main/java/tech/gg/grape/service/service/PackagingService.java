package tech.gg.grape.service.service;

import tech.gg.grape.repository.model.Bottle;
import tech.gg.grape.repository.model.Packaging;

import java.util.List;

public interface PackagingService {

    List<Packaging> findAll();

    Packaging findOne(long id);

    Packaging save(Packaging packaging);

    Packaging changeQuantity(Packaging packaging, double quantity);

    Bottle save(Bottle bottle);

    void delete(Packaging packaging);


    Packaging findOneById(int packagingId);

    Bottle findBottleForPackagingId(Long packagingId);

    Bottle findBottleById(int packagingId);

    Packaging findByNameAndCategoryAndAmount(String name, int category, double amount);
}
