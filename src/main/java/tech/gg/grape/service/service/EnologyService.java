package tech.gg.grape.service.service;

import tech.gg.grape.repository.model.Enology;
import tech.gg.grape.repository.model.EnologyCategory;

import java.util.List;

public interface EnologyService {

    Enology findOne(long id);

    Enology save(Enology enology);

    Enology changeQuantity(Enology enology, double quantity);

    void delete(Enology id);

    List<Enology> findAll();

    Enology findByNameAndCategoryAndAmount(String name, int category, double amount);

    List<EnologyCategory> findAllCategories();

    void saveCategory(EnologyCategory category);

    EnologyCategory findCategory(int category);

}
