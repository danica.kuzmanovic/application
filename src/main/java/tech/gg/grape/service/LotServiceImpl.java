package tech.gg.grape.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.gg.grape.repository.LotRepository;
import tech.gg.grape.repository.SpentEnologyRepository;
import tech.gg.grape.repository.SpentMaterialRepository;
import tech.gg.grape.repository.SpentPackagingRepository;
import tech.gg.grape.repository.model.Lot;
import tech.gg.grape.repository.model.SpentEnology;
import tech.gg.grape.repository.model.SpentMaterial;
import tech.gg.grape.repository.model.SpentPackaging;
import tech.gg.grape.service.service.LotService;

import java.util.List;

@Service
public class LotServiceImpl implements LotService {

    @Autowired
    LotRepository lotRepository;

    @Autowired
    SpentEnologyRepository spentEnologyRepository;

    @Autowired
    SpentMaterialRepository spentMaterialRepository;

    @Autowired
    SpentPackagingRepository spentPackagingRepository;

    @Override
    public Lot findOne(long id) {
        return lotRepository.findOneById(id);
    }

    @Override
    public List<Lot> findAll() {
        return lotRepository.findAll();
    }

    @Override
    public Lot findOneByTankId(long id) {
        return lotRepository.findOneByTankId(id);
    }

    @Override
    public Lot save(Lot lot) {
        return lotRepository.save(lot);
    }

    @Override
    public List<SpentMaterial> findAllSpentMaterial(long lotId) {
        return spentMaterialRepository.findByLotId(lotId);
    }

    @Override
    public List<SpentEnology> findAllSpentEnology(long lotId) {
        return spentEnologyRepository.findAllByLotId(lotId);
    }

    @Override
    public List<SpentPackaging> findAllSpentPackaging(long lotId) {
        return spentPackagingRepository.findByLotId(lotId);
    }

    @Override
    public Long findTankIdByUser(long id) {
        return lotRepository.findTankId(id);
    }

    @Override
    public Lot findOneById(long lotId) {
        return lotRepository.findOneById(lotId);
    }
}
