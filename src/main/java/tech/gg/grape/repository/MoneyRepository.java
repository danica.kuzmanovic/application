package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.Money;

import java.util.Optional;

@Repository
public interface MoneyRepository extends JpaRepository<Money, Long> {

    @Query(value = "SELECT * FROM money WHERE amount = :amount AND unit = :unit", nativeQuery = true)
    Optional<Money> findByAmountAndUnit(double amount, int unit);
}
