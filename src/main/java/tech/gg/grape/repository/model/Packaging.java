package tech.gg.grape.repository.model;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Packaging {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated
    private PackagingCategory category;

    @Column
    private String name;

    @Column
    private double quantity;

    @ManyToOne
    @JoinColumn(name = "money_id")
    private Money money;


    @Column
    private boolean deleted = false;

    public Packaging() {
    }


    public Packaging(PackagingCategory category, String name, double quantity, Money money) {
        this.category = category;
        this.name = name;
        this.quantity = quantity;
        this.money = money;
    }

    public Packaging(Long id, PackagingCategory category, String name, double quantity, Money money) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.quantity = quantity;
        this.money = money;
    }

    public Packaging(long id, PackagingCategory category, String name, double quantity, Money money) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.quantity = quantity;
        this.money = money;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PackagingCategory getCategory() {
        return category;
    }

    public void setCategory(PackagingCategory category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
