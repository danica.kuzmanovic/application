package tech.gg.grape.repository.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Fermentation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private Double sugar;

    @Column
    private Double temperature;

    @Column
    private Double alcohol;

    @Column
    private Double acid;

    @Column
    private Double pH;

    @Column
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name = "lot_id")
    private Lot lot;

    public Fermentation() {
    }

    public Fermentation(Double sugar, Double temperature, Double alcohol, Double acid, Double pH, LocalDate date, Lot lot) {
        this.sugar = sugar;
        this.temperature = temperature;
        this.alcohol = alcohol;
        this.acid = acid;
        this.pH = pH;
        this.date = date;
        this.lot = lot;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getSugar() {
        return sugar;
    }

    public void setSugar(Double sugar) {
        this.sugar = sugar;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(Double alcohol) {
        this.alcohol = alcohol;
    }

    public Double getAcid() {
        return acid;
    }

    public void setAcid(Double acid) {
        this.acid = acid;
    }

    public Double getpH() {
        return pH;
    }

    public void setpH(Double pH) {
        this.pH = pH;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Lot getLot() {
        return lot;
    }

    public void setLot(Lot lot) {
        this.lot = lot;
    }
}
