package tech.gg.grape.repository.model;

import javax.persistence.*;



@Entity
public class Enology {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;

    @Column
    private double quantity;

    @ManyToOne
    @JoinColumn(name = "categoryId")
    private EnologyCategory category;

    @ManyToOne
    @JoinColumn(name = "money_id")
    private Money money;

    @Column
    private boolean deleted = false;

    public Money getMoney() {
        return money;
    }

    public Enology() {
    }

    public Enology(String name, double quantity, EnologyCategory category, Money money) {
        this.name = name;
        this.quantity = quantity;
        this.category = category;
        this.money = money;

    }

    public Enology(long id, String name, double quantity, EnologyCategory category, Money money) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.category = category;
        this.money = money;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public EnologyCategory getCategory() {
        return category;
    }

    public void setCategory(EnologyCategory category) {
        this.category = category;
    }

    public void setMoney(Money money) {
        this.money = money;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

}
