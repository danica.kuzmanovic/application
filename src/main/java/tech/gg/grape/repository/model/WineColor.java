package tech.gg.grape.repository.model;

public enum WineColor {
    RED,
    WHITE,
    ROSE,
    ORANGE
}
