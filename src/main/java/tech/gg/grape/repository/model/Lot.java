package tech.gg.grape.repository.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Lot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String lotName;

    @Column
    private LocalDate startTime;

    @Column
    private LocalDate expectedEndDate;

    @Column
    private String wineLabel;

    @Enumerated
    private WineColor wineColor;

    @Column
    private double usedCapacity = 0;

    @OneToOne
    @JoinColumn(name = "tankId")
    private Tank tank;


    public Lot() {
    }

    public Lot(long id, String lotName, LocalDate startTime, LocalDate expectedEndDate, String wineLabel, WineColor wineColor, int usedCapacity, Tank tank) {
        this.id = id;
        this.lotName = lotName;
        this.startTime = startTime;
        this.expectedEndDate = expectedEndDate;
        this.wineLabel = wineLabel;
        this.wineColor = wineColor;
        this.usedCapacity = usedCapacity;
        this.tank = tank;
    }

    public Lot(String lotName, LocalDate startTime, LocalDate expectedEndDate, String wineLabel, WineColor wineColor,
               int usedCapacity, Tank tank) {
        this.lotName = lotName;
        this.startTime = startTime;
        this.expectedEndDate = expectedEndDate;
        this.wineLabel = wineLabel;
        this.wineColor = wineColor;
        this.usedCapacity = usedCapacity;
        this.tank = tank;
    }

    public Lot(long id, String lotName, LocalDate startTime, LocalDate expectedEndDate, String wineLabel,
               WineColor wineColor, double usedCapacity, Tank tank) {
        this.id = id;
        this.lotName = lotName;
        this.startTime = startTime;
        this.expectedEndDate = expectedEndDate;
        this.wineLabel = wineLabel;
        this.wineColor = wineColor;
        this.usedCapacity = usedCapacity;
        this.tank = tank;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public LocalDate getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDate startTime) {
        this.startTime = startTime;
    }

    public LocalDate getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(LocalDate expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public String getWineLabel() {
        return wineLabel;
    }

    public void setWineLabel(String wineLabel) {
        this.wineLabel = wineLabel;
    }

    public WineColor getWineColor() {
        return wineColor;
    }

    public void setWineColor(WineColor wineColor) {
        this.wineColor = wineColor;
    }

    public double getUsedCapacity() {
        return usedCapacity;
    }

    public void setUsedCapacity(double usedCapacity) {
        this.usedCapacity = usedCapacity;
    }

    public Tank getTank() {
        return tank;
    }

    public void setTank(Tank tank) {
        this.tank = tank;
    }

}
