package tech.gg.grape.repository.model;

import javax.persistence.*;

@Entity
public class EnologyCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;


    public EnologyCategory(String name) {
        this.name = name;
    }

    public EnologyCategory() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
