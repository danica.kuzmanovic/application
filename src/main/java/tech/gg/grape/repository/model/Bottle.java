package tech.gg.grape.repository.model;

import javax.persistence.*;

@Entity
public class Bottle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private int volume;

    @Enumerated
    private VolumeUnit volumeUnit;

    @OneToOne
    @JoinColumn(name = "packaging_id")
    private Packaging packaging;

    public Bottle() {
    }

    public Bottle(int volume, VolumeUnit volumeUnit, Packaging packaging) {
        this.volume = volume;
        this.volumeUnit = volumeUnit;
        this.packaging = packaging;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public VolumeUnit getVolumeUnit() {
        return volumeUnit;
    }

    public void setVolumeUnit(VolumeUnit volumeUnit) {
        this.volumeUnit = volumeUnit;
    }

    public Packaging getPackaging() {
        return packaging;
    }

    public void setPackaging(Packaging packaging) {
        this.packaging = packaging;
    }
}
