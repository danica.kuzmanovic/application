package tech.gg.grape.repository.model;

import javax.persistence.*;


@Entity
public class Tank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private long orderNumber;

    @Column
    private int capacity;

    @Enumerated
    private TankType tankType;


    public Tank() {
    }

    public Tank(long orderNumber, int capacity, TankType tankType) {
        this.orderNumber = orderNumber;
        this.capacity = capacity;
        this.tankType = tankType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public TankType getTankType() {
        return tankType;
    }

    public void setTankType(TankType tankType) {
        this.tankType = tankType;
    }

}
