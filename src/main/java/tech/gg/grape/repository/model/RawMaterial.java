package tech.gg.grape.repository.model;

import javax.persistence.*;

@Entity
public class RawMaterial {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;

    @Column
    private double quantity;

    @ManyToOne
    @JoinColumn(name = "money_id")
    private Money money;

    @Column
    private boolean deleted = false;

    public RawMaterial() {

    }

    public RawMaterial(String name, double quantity, Money money) {
        this.name = name;
        this.quantity = quantity;
        this.money = money;

    }

    public RawMaterial(Long id, String name, double quantity, Money money) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.money = money;
    }

    public RawMaterial(long id, String name, double quantity, Money money) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.money = money;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
