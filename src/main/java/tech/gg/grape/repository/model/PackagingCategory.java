package tech.gg.grape.repository.model;

public enum PackagingCategory {
    BOTTLE,
    BOTTLE_CAP,
    LABEL,
    DECORATIVE_CAP
}
