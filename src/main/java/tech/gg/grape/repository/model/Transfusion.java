package tech.gg.grape.repository.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Transfusion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private long sourceTankId;

    @Column
    private long targetTankId;

    @Column
    private long lotId;

    @Column
    private double wineAmount;

    @Column
    private double wasteAmount;

    @Column
    private LocalDate date;

    @Column
    private String comment;

    public Transfusion() {
    }

    public Transfusion(long sourceTankId, long targetTankId, long lotId, double wineAmount, double wasteAmount, LocalDate date, String comment) {
        this.sourceTankId = sourceTankId;
        this.targetTankId = targetTankId;
        this.lotId = lotId;
        this.wineAmount = wineAmount;
        this.wasteAmount = wasteAmount;
        this.date = date;
        this.comment = comment;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSourceTankId() {
        return sourceTankId;
    }

    public void setSourceTankId(long sourceTankId) {
        this.sourceTankId = sourceTankId;
    }

    public long getTargetTankId() {
        return targetTankId;
    }

    public void setTargetTankId(long targetTankId) {
        this.targetTankId = targetTankId;
    }

    public long getLotId() {
        return lotId;
    }

    public void setLotId(long lotId) {
        this.lotId = lotId;
    }

    public double getWineAmount() {
        return wineAmount;
    }

    public void setWineAmount(double wineAmount) {
        this.wineAmount = wineAmount;
    }

    public double getWasteAmount() {
        return wasteAmount;
    }

    public void setWasteAmount(double wasteAmount) {
        this.wasteAmount = wasteAmount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
