package tech.gg.grape.repository.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class SpentEnology {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "enology_id")
    private Enology enology;

    @ManyToOne
    @JoinColumn(name = "lot_id")
    private Lot lot;

    @Column
    private LocalDate date;

    @Column
    private String comment = "";

    public SpentEnology() {

    }

    public SpentEnology(int quantity, Enology enology, Lot lot, LocalDate date, String comment) {
        this.quantity = quantity;
        this.enology = enology;
        this.lot = lot;
        this.date = date;
        this.comment = comment;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Enology getEnology() {
        return enology;
    }

    public void setEnology(Enology enology) {
        this.enology = enology;
    }

    public Lot getLot() {
        return lot;
    }

    public void setLot(Lot lot) {
        this.lot = lot;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
