package tech.gg.grape.repository.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class SulfurMeasurement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private double tso2;

    @Column
    private double fso2;

    @Column
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name = "lot_Id")
    private Lot lot;

    public SulfurMeasurement() {
    }

    public SulfurMeasurement(double tso2, double fso2, LocalDate date, Lot lot) {
        this.tso2 = tso2;
        this.fso2 = fso2;
        this.date = date;
        this.lot = lot;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getTso2() {
        return tso2;
    }

    public void setTso2(double tso2) {
        this.tso2 = tso2;
    }

    public double getFso2() {
        return fso2;
    }

    public void setFso2(double fso2) {
        this.fso2 = fso2;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Lot getLot() {
        return lot;
    }

    public void setLot(Lot lot) {
        this.lot = lot;
    }
}
