package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.SulfurMeasurement;

import java.util.List;

@Repository
public interface SulfurFermentationRepository extends JpaRepository<SulfurMeasurement, Long> {

    @Query(value = "SELECT * FROM sulfur_measurement WHERE lot_id = :lotId ORDER BY date DESC LIMIT 1", nativeQuery = true)
    List<SulfurMeasurement> getSulfurMeasurementByLotId(long lotId);
}
