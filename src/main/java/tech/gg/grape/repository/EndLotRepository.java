package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.EndLot;

@Repository
public interface EndLotRepository extends JpaRepository<EndLot, Long> {
    EndLot findByLotId(long id);
}
