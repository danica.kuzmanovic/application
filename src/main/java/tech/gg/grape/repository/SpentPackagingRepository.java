package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.SpentPackaging;

import java.util.List;

@Repository
public interface SpentPackagingRepository extends JpaRepository<SpentPackaging, Long> {

    List<SpentPackaging> findByLotId(long lotId);
}
