package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.EnologyCategory;

import java.util.List;

@Repository
public interface EnologyCategoryRepository extends JpaRepository<EnologyCategory, Integer> {

    EnologyCategory findById(int category);

    EnologyCategory save(int id);

    List<EnologyCategory> findAll();

//    List<Enology> findAllByUserIdAndDeletedFalse(long userId);


}
