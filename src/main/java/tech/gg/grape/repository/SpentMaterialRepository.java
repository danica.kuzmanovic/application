package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.SpentMaterial;

import java.util.List;

@Repository
public interface SpentMaterialRepository extends JpaRepository<SpentMaterial, Long> {

    List<SpentMaterial> findByLotId(long lotId);
}
