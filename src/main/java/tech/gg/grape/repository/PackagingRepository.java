package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.Packaging;

import java.util.List;

@Repository
public interface PackagingRepository extends JpaRepository<Packaging, Long> {

   Packaging findOneById(long id);

    @Query(value = "SELECT * FROM packaging JOIN money ON money_id = money.id WHERE name = :name AND " +
            "category = :category AND amount = :amount", nativeQuery = true)
    Packaging findByNameAndCategoryAndAmount(String name, int category, double amount);

    List<Packaging> findByQuantityLessThanAndDeletedFalse(double quantity);

    List<Packaging> findByDeletedFalse();
}
