package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.Transfusion;

import java.util.List;

@Repository
public interface TransfusionRepository extends JpaRepository<Transfusion, Long> {
    List<Transfusion> findAllByLotId(long id);
}
