package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.RawMaterial;

import java.util.List;

@Repository
public interface RawMaterialRepository extends JpaRepository<RawMaterial, Long> {

    List<RawMaterial> findByQuantityLessThanAndDeletedFalse(double quantity);

    List<RawMaterial> findAllByDeletedFalse();

    RawMaterial findOneById(long id);

    @Query(value = "SELECT * FROM raw_material JOIN money ON money_id = money.id WHERE name = :name " +
            "AND amount = :amount", nativeQuery = true)
    RawMaterial findByNameAndAmount(String name, double amount);
}
