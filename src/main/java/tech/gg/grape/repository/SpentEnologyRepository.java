package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.SpentEnology;

import java.util.List;

@Repository
public interface SpentEnologyRepository extends JpaRepository<SpentEnology, Long> {

    List<SpentEnology> findAllByLotId(long lotId);
}
