package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.Fermentation;

import java.util.List;

@Repository
public interface FermentationRepository extends JpaRepository<Fermentation, Long> {

    @Query(value = "SELECT * FROM fermentation WHERE lot_id = :lotId ORDER BY id DESC LIMIT 1", nativeQuery = true)
    List<Fermentation> getFermentationByLotId(long lotId);

    List<Fermentation> findAllByLotId(long id);

    Fermentation findById(long id);
}
