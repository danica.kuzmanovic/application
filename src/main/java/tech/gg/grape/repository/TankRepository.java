package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.Tank;

import java.util.List;

@Repository
public interface TankRepository extends JpaRepository<Tank, Long> {

    Tank findById(long id);

    Tank getById(long id);

    List<Tank> findAll();

    Tank findByOrderNumber(long orderNumber);
}
