package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.Bottle;

import java.util.List;

@Repository
public interface BottleRepository extends JpaRepository<Bottle, Long> {

    List<Bottle> findAll();

    Bottle findByPackagingId(long packagingId);
}
