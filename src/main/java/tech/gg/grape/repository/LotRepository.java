package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tech.gg.grape.repository.model.Lot;

import java.util.List;

public interface LotRepository extends JpaRepository<Lot, Long> {

    List<Lot> findAll();

    Lot findOneById(long id);

    Lot findOneByTankId(long id);

    @Query(value = "SELECT tank_id FROM lot WHERE  tank_id = :tankId", nativeQuery = true)
    Long findTankId(long tankId);

}
