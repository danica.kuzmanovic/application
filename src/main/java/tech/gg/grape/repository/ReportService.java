package tech.gg.grape.repository;

import java.io.IOException;

public interface ReportService {
    String createDocument(long userId) throws IOException;
}
