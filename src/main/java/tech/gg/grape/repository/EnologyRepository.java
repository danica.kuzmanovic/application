package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.Enology;

import java.util.List;

@Repository
public interface EnologyRepository extends JpaRepository<Enology, Long> {

    List<Enology> findByQuantityLessThanAndDeletedFalse(double quantity);

    List<Enology> findAllByDeletedFalse();

    Enology findOneById(long id);

    @Query(value = "SELECT * FROM enology JOIN money ON money_id = money.id WHERE name = :name AND category_id = :category " +
            "AND amount = :amount", nativeQuery = true)
    Enology findByNameAndCategoryAndAmount(String name, int category, double amount);

}
