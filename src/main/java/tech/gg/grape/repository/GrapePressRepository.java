package tech.gg.grape.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.gg.grape.repository.model.GrapePress;

import java.util.List;

@Repository
public interface GrapePressRepository extends JpaRepository<GrapePress, Long> {
    List<GrapePress> findAllByLotId(long id);
}
