package tech.gg.grape.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tech.gg.grape.controller.dto.*;
import tech.gg.grape.service.service.PackagingService;
import tech.gg.grape.repository.model.Bottle;
import tech.gg.grape.repository.model.Packaging;
import tech.gg.grape.service.support.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/grappe/packaging", produces = MediaType.APPLICATION_JSON_VALUE)
public class PackagingController {

    @Autowired
    PackagingService packagingService;

    @Autowired
    PackagingToPackagingDtoResponse toPackagingDto;

    @Autowired
    PackagingDtoRequestToPackaging toPackaging;

    @Autowired
    PackagingBottleDtoToPackaging bottleDtoToPackaging;

    @Autowired
    PackagingBottleToPackagingBottleDto toPackagingBottleDto;

    @Autowired
    PackagingBottleDtoRequestToBottle toBottle;

    @GetMapping
    public ResponseEntity<PackagingDtoResponse> getAll() {

        List<Packaging> packagings = packagingService.findAll();
        List<Bottle> bottles = packagings.stream()
                .map(packaging -> packagingService.findBottleForPackagingId(packaging.getId()))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        PackagingDtoResponse packagingDtoResponse = new PackagingDtoResponse(toPackagingBottleDto.convert(bottles),
                toPackagingDto.convert(packagings));
        return new ResponseEntity<>(packagingDtoResponse, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<HttpStatus> create(@Validated @RequestBody PackagingDtoRequest packagingDtoRequest) {

        Packaging packaging = packagingService.findByNameAndCategoryAndAmount(packagingDtoRequest.getName(),
                packagingDtoRequest.getCategory(), packagingDtoRequest.getAmount());
        if (packaging != null && !packaging.isDeleted()) {
            packagingService.save(new Packaging(packaging.getId(), packaging.getCategory(), packaging.getName(),
                    packaging.getQuantity(), packaging.getMoney()));
        } else if (packaging != null && packaging.isDeleted()) {
            packaging.setQuantity(packagingDtoRequest.getQuantity());
            packaging.setDeleted(false);
            packagingService.save(packaging);
        } else {
            packagingService.save(toPackaging.convert(packagingDtoRequest));
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping(value = "/bottle")
    public ResponseEntity<HttpStatus> create(@Validated @RequestBody PackagingBottleDtoRequest packagingBottleDtoRequest) {


        if (packagingBottleDtoRequest.getCategory() != 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Packaging packaging = packagingService.findByNameAndCategoryAndAmount(packagingBottleDtoRequest.getName(),
                packagingBottleDtoRequest.getCategory(), packagingBottleDtoRequest.getAmount());
        if (packaging != null && !packaging.isDeleted()) {
            packagingService.save(new Packaging(packaging.getId(), packaging.getCategory(), packaging.getName(),
                    packaging.getQuantity() + packagingBottleDtoRequest.getQuantity(), packaging.getMoney()));
        } else if (packaging != null && packaging.isDeleted()) {
            packaging.setDeleted(false);
            packaging.setQuantity(packagingBottleDtoRequest.getQuantity());
            packagingService.save(packaging);
        } else {
            Packaging newPackaging = bottleDtoToPackaging.convert(packagingBottleDtoRequest);
            packagingService.save(newPackaging);
            Bottle bottle = toBottle.convert(packagingBottleDtoRequest, newPackaging);
            packagingService.save(bottle);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable long id) {

        Packaging packaging = packagingService.findOne(id);
        if (packaging != null) {
            packagingService.delete(packaging);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PatchMapping(value = "/changeQuantity")
    public ResponseEntity<PackagingDto> changeQuantity(@RequestBody StorageItem storageItem) {

        if (storageItem.getEntityId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Packaging packaging = packagingService.findOne(storageItem.getEntityId());
        if (packaging != null) {
            packagingService.changeQuantity(packaging, storageItem.getQuantity());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}