package tech.gg.grape.controller.dto;

public class StorageItemDatabaseState {

    private String name;
    private double quantity;

    public StorageItemDatabaseState(String name, double quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }
}
