package tech.gg.grape.controller.dto;

import javax.validation.constraints.Min;

public class PackagingBottleDtoRequest {

    private int category;
    private String name;
    @Min(1)
    private double quantity;
    @Min(1)
    private int volume;
    private int volumeUnit;
    @Min(1)
    private double amount;
    private int unit;

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getVolumeUnit() {
        return volumeUnit;
    }

    public void setVolumeUnit(int volummmeUnit) {
        this.volumeUnit = volummmeUnit;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }
}
