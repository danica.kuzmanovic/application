package tech.gg.grape.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class SulfurMeasurementDto {

    private Long lotId;
    private double fso2;
    private double tso2;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    public SulfurMeasurementDto() {
    }

    public SulfurMeasurementDto(Long lotId, double fso2, double tso2, LocalDate date) {
        this.lotId = lotId;
        this.fso2 = fso2;
        this.tso2 = tso2;
        this.date = date;
    }

    public Long getLotId() {
        return lotId;
    }

    public void setLotId(Long lotId) {
        this.lotId = lotId;
    }

    public double getFso2() {
        return fso2;
    }

    public void setFso2(double fso2) {
        this.fso2 = fso2;
    }

    public double getTso2() {
        return tso2;
    }

    public void setTso2(double tso2) {
        this.tso2 = tso2;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
