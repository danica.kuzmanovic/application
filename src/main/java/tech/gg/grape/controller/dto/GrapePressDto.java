package tech.gg.grape.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Min;
import java.time.LocalDate;

public class GrapePressDto {

    private int lotId;
    @Min(1)
    private int startingVolume;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    private String comment;

    public GrapePressDto() {
    }

    public int getLotId() {
        return lotId;
    }

    public void setLotId(int lotId) {
        this.lotId = lotId;
    }

    public int getStartingVolume() {
        return startingVolume;
    }

    public void setStartingVolume(int startingVolume) {
        this.startingVolume = startingVolume;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
