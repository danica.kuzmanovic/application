package tech.gg.grape.controller.dto;

public class EnologyCategoryPostDto {

    private String name;

    public EnologyCategoryPostDto() {
    }

    public EnologyCategoryPostDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
