package tech.gg.grape.controller.dto;

public class TankDetailsDto {

    private String lotName;

    private int usedCapacity;

    private int wineColor;

    public TankDetailsDto(String lotName, int usedCapacity, int wineColor) {
        this.lotName = lotName;
        this.usedCapacity = usedCapacity;
        this.wineColor = wineColor;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public int getUsedCapacity() {
        return usedCapacity;
    }

    public void setUsedCapacity(int usedCapacity) {
        this.usedCapacity = usedCapacity;
    }

    public int getWineColor() {
        return wineColor;
    }

    public void setWineColor(int wineColor) {
        this.wineColor = wineColor;
    }
}
