package tech.gg.grape.controller.dto;

public class ClosedLotDto {

    private int lotId;
    private String lotName;
    private String endDate;

    public ClosedLotDto(int lotId, String lotName, String endDate) {
        this.lotId = lotId;
        this.lotName = lotName;
        this.endDate = endDate;
    }

    public int getLotId() {
        return lotId;
    }

    public void setLotId(int lotId) {
        this.lotId = lotId;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
