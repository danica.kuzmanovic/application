package tech.gg.grape.controller.dto;

public class LotDto {

    private Long lotId;

    private String lotName;

    public LotDto() {
    }


    public LotDto(Long lotId, String lotName) {
        this.lotId = lotId;
        this.lotName = lotName;
    }

    public Long getLotId() {
        return lotId;
    }

    public void setLotId(Long lotId) {
        this.lotId = lotId;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

}
