package tech.gg.grape.controller.dto;

public class TankDtoResponse {

    private long id;
    private long orderNumber;
    private int capacity;
    private int tankType;
    private TankDetailsDto tankDetailsDto;

    public TankDtoResponse(long id, long orderNumber, int capacity, int tankType, TankDetailsDto tankDetailsDto) {
        this.id = id;
        this.orderNumber = orderNumber;
        this.capacity = capacity;
        this.tankType = tankType;
        this.tankDetailsDto = tankDetailsDto;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getTankType() {
        return tankType;
    }

    public void setTankType(int tankType) {
        this.tankType = tankType;
    }

    public TankDetailsDto getTankDetailsDto() {
        return tankDetailsDto;
    }

    public void setTankDetailsDto(TankDetailsDto tankDetailsDto) {
        this.tankDetailsDto = tankDetailsDto;
    }
}
