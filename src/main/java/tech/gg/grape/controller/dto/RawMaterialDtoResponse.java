package tech.gg.grape.controller.dto;

public class RawMaterialDtoResponse {

    private long id;
    private String name;
    private double quantity;
    private double amount;
    private int unit;

    public RawMaterialDtoResponse(long id, String name, double quantity, double amount, int unit) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.amount = amount;
        this.unit = unit;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }
}
