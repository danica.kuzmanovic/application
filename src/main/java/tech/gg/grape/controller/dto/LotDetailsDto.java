package tech.gg.grape.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class LotDetailsDto {
    private int lotId;
    private String lotName;
    private String wineLabel;
    private int wineColor;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate expectedEndDate;
    private long tankOrderNumber;
    private int usedCapacity;
    private int maxCapacity;

    public LotDetailsDto() {
    }

    public LotDetailsDto(int lotId, String lotName, String wineLabel, int wineColor, LocalDate startDate, LocalDate expectedEndDate, long tankOrderNumber, int usedCapacity, int maxCapacity) {
        this.lotId = lotId;
        this.lotName = lotName;
        this.wineLabel = wineLabel;
        this.wineColor = wineColor;
        this.startDate = startDate;
        this.expectedEndDate = expectedEndDate;
        this.tankOrderNumber = tankOrderNumber;
        this.usedCapacity = usedCapacity;
        this.maxCapacity = maxCapacity;
    }

    public int getLotId() {
        return lotId;
    }

    public void setLotId(int lotId) {
        this.lotId = lotId;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public String getWineLabel() {
        return wineLabel;
    }

    public void setWineLabel(String wineLabel) {
        this.wineLabel = wineLabel;
    }

    public int getWineColor() {
        return wineColor;
    }

    public void setWineColor(int wineColor) {
        this.wineColor = wineColor;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(LocalDate expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public long getTankOrderNumber() {
        return tankOrderNumber;
    }

    public void setTankOrderNumber(long tankOrderNumber) {
        this.tankOrderNumber = tankOrderNumber;
    }

    public int getUsedCapacity() {
        return usedCapacity;
    }

    public void setUsedCapacity(int usedCapacity) {
        this.usedCapacity = usedCapacity;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }
}
