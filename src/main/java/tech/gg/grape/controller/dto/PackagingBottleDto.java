package tech.gg.grape.controller.dto;

public class PackagingBottleDto {

    private long id;
    private int volume;
    private int volumeUnit;
    private long packagingId;

    public PackagingBottleDto(long id, int volume, int volumeUnit, long packagingId) {
        this.id = id;
        this.volume = volume;
        this.volumeUnit = volumeUnit;
        this.packagingId = packagingId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getVolumeUnit() {
        return volumeUnit;
    }

    public void setVolumeUnit(int volumeUnit) {
        this.volumeUnit = volumeUnit;
    }

    public long getPackagingId() {
        return packagingId;
    }

    public void setPackagingId(long packagingId) {
        this.packagingId = packagingId;
    }
}
