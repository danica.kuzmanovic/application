package tech.gg.grape.controller.dto;

import java.util.List;

public class AllClosedLotsDto {

    List<ClosedLotDto> closedLotList;

    public AllClosedLotsDto(List<ClosedLotDto> closedLotList) {
        this.closedLotList = closedLotList;
    }

    public List<ClosedLotDto> getClosedLotList() {
        return closedLotList;
    }

    public void setClosedLotList(List<ClosedLotDto> closedLotList) {
        this.closedLotList = closedLotList;
    }
}

