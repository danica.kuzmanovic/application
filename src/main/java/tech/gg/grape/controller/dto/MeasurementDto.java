package tech.gg.grape.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class MeasurementDto {

    private int lotId;
    private Double sugar;
    private Double temperature;
    private Double alcohol;
    private Double acid;
    private Double pH;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    public MeasurementDto() {
    }

    public MeasurementDto(int lotId, Double sugar, Double temperature, Double alcohol, Double acid, Double pH, LocalDate date) {

        this.lotId = lotId;
        this.sugar = sugar;
        this.temperature = temperature;
        this.alcohol = alcohol;
        this.acid = acid;
        this.pH = pH;
        this.date = date;
    }

    public int getLotId() {
        return lotId;
    }

    public void setLotId(int lotId) {
        this.lotId = lotId;
    }

    public Double getSugar() {
        return sugar;
    }

    public void setSugar(Double sugar) {
        this.sugar = sugar;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(Double alcohol) {
        this.alcohol = alcohol;
    }

    public Double getAcid() {
        return acid;
    }

    public void setAcid(Double acid) {
        this.acid = acid;
    }

    public Double getpH() {
        return pH;
    }

    public void setpH(Double pH) {
        this.pH = pH;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
