package tech.gg.grape.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class CreateLotDto {

    private long tankId;

    private String lotName;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate expectedEndDate;

    private String wineLabel;

    private int wineColor;

    public CreateLotDto() {
    }

    public long getTankId() {
        return tankId;
    }

    public void setTankId(long tankId) {
        this.tankId = tankId;
    }

    public String getLotName() {
        return lotName;
    }

    public void setLotName(String lotName) {
        this.lotName = lotName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(LocalDate expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public String getWineLabel() {
        return wineLabel;
    }

    public void setWineLabel(String wineLabel) {
        this.wineLabel = wineLabel;
    }

    public int getWineColor() {
        return wineColor;
    }

    public void setWineColor(int wineColor) {
        this.wineColor = wineColor;
    }
}
