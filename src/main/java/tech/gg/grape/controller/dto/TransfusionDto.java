package tech.gg.grape.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class TransfusionDto {

    private long targetTankId;
    private long lotId;
    private double wineAmount;
    private double wasteAmount;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    private String comment;

    public long getTargetTankId() {
        return targetTankId;
    }

    public void setTargetTankId(long targetTankId) {
        this.targetTankId = targetTankId;
    }

    public long getLotId() {
        return lotId;
    }

    public void setLotId(long lotId) {
        this.lotId = lotId;
    }

    public double getWineAmount() {
        return wineAmount;
    }

    public void setWineAmount(double wineAmount) {
        this.wineAmount = wineAmount;
    }

    public double getWasteAmount() {
        return wasteAmount;
    }

    public void setWasteAmount(double wasteAmount) {
        this.wasteAmount = wasteAmount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
