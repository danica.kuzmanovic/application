package tech.gg.grape.controller.dto;

import java.util.List;

public class AllLotsDto {

    private List<LotDetailsDto> lots;

    public AllLotsDto() {
    }

    public AllLotsDto(List<LotDetailsDto> lots) {
        this.lots = lots;
    }

    public List<LotDetailsDto> getLots() {
        return lots;
    }

    public void setLots(List<LotDetailsDto> lots) {
        this.lots = lots;
    }
}
