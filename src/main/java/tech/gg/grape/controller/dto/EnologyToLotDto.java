package tech.gg.grape.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class EnologyToLotDto {

    private int lotId;
    private int enologyId;
    private int quantity;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    private String comment;

    public EnologyToLotDto() {
    }

    public int getLotId() {
        return lotId;
    }

    public void setLotId(int lotId) {
        this.lotId = lotId;
    }

    public int getEnologyId() {
        return enologyId;
    }

    public void setEnologyId(int enologyId) {
        this.enologyId = enologyId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
