package tech.gg.grape.controller.dto;

public class TankDtoRequest {

    private long orderNumber;
    private int capacity;
    private int tankType;

    public void setOrderNumber(long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setTankType(int tankType) {
        this.tankType = tankType;
    }

    public long getOrderNumber() {
        return orderNumber;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getTankType() {
        return tankType;
    }
}
