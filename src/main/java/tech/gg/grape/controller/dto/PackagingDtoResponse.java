package tech.gg.grape.controller.dto;

import java.util.List;

public class PackagingDtoResponse {

    private List<PackagingBottleDto> bottleDtos;
    private List<PackagingDto> packagingDtos;

    public PackagingDtoResponse(List<PackagingBottleDto> bottleDtos, List<PackagingDto> packagingDtos) {
        this.bottleDtos = bottleDtos;
        this.packagingDtos = packagingDtos;
    }

    public List<PackagingBottleDto> getBottleDtos() {
        return bottleDtos;
    }

    public void setBottleDtos(List<PackagingBottleDto> bottleDtos) {
        this.bottleDtos = bottleDtos;
    }

    public List<PackagingDto> getPackagingDtos() {
        return packagingDtos;
    }

    public void setPackagingDtos(List<PackagingDto> packagingDtos) {
        this.packagingDtos = packagingDtos;
    }
}
