package tech.gg.grape.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tech.gg.grape.controller.dto.RawMaterialDtoRequest;
import tech.gg.grape.controller.dto.RawMaterialDtoResponse;
import tech.gg.grape.controller.dto.StorageItem;
import tech.gg.grape.service.service.RawMaterialService;
import tech.gg.grape.repository.model.RawMaterial;
import tech.gg.grape.repository.model.Unit;
import tech.gg.grape.service.support.RawMaterialDtoToRawMaterial;
import tech.gg.grape.service.support.RawMaterialToRawMaterialDto;

import java.util.List;

@RestController
@RequestMapping(value = "/grappe/rawMaterial", produces = MediaType.APPLICATION_JSON_VALUE)
public class RawMaterialController {

    @Autowired
    RawMaterialToRawMaterialDto toRawMaterialDto;

    @Autowired
    RawMaterialDtoToRawMaterial toRawMaterial;

    @Autowired
    RawMaterialService rawMaterialService;

    @GetMapping
    public ResponseEntity<List<RawMaterialDtoResponse>> getAll() {

        List<RawMaterial> rawMaterials = rawMaterialService.findAll();
        return new ResponseEntity<>(toRawMaterialDto.convert(rawMaterials), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<RawMaterialDtoResponse> getOne(@PathVariable("id") Long id) {
;
        RawMaterial rawMaterial = rawMaterialService.findOne(id);
        if (rawMaterial != null && !rawMaterial.isDeleted()) {
            return new ResponseEntity<>(toRawMaterialDto.convert(rawMaterial), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> create(@Validated @RequestBody RawMaterialDtoRequest rawMaterialDto) {
        if (rawMaterialDto.getUnit() >= Unit.values().length){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        RawMaterial rawMaterial = rawMaterialService.findByNameAndAmount(rawMaterialDto.getName(), rawMaterialDto.getAmount());
        if (rawMaterial != null && !rawMaterial.isDeleted()) {
            rawMaterialService.save(new RawMaterial(rawMaterial.getId(), rawMaterial.getName(),
                    rawMaterial.getQuantity() + rawMaterialDto.getQuantity(), rawMaterial.getMoney()));
        } else if (rawMaterial != null && rawMaterial.isDeleted()) {
            rawMaterial.setDeleted(false);
            rawMaterial.setQuantity(rawMaterialDto.getQuantity());
            rawMaterialService.save(rawMaterial);
        } else {
            rawMaterialService.save(toRawMaterial.convert(rawMaterialDto));
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable long id) {

        RawMaterial rawMaterial = rawMaterialService.findOne(id);
        if (rawMaterial != null) {
            rawMaterialService.delete(rawMaterial);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PatchMapping(value = "/changeQuantity")
    public ResponseEntity<RawMaterialDtoResponse> changeQuantity(@RequestBody StorageItem storageItem) {

        RawMaterial rawMaterial = rawMaterialService.findOne(storageItem.getEntityId());
        if (rawMaterial != null) {
            RawMaterial updated = rawMaterialService.changeQuantity(rawMaterial, storageItem.getQuantity());
            return new ResponseEntity<>(toRawMaterialDto.convert(updated), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}

