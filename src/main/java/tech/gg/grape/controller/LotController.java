package tech.gg.grape.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tech.gg.grape.controller.dto.*;
import tech.gg.grape.repository.ReportService;
import tech.gg.grape.repository.model.*;
import tech.gg.grape.service.service.*;
import tech.gg.grape.service.support.*;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/grappe/lot")
public class LotController {

    @Autowired
    private LotService lotService;

    @Autowired
    private RawMaterialService rawMaterialService;

    @Autowired
    private EnologyService enologyService;

    @Autowired
    private StorageSpentService storageSpentService;

    @Autowired
    private PackagingService packagingService;

    @Autowired
    private TransfusionService transfusionService;

    @Autowired
    private TankService tankService;

    @Autowired
    private EndLotService endLotService;

    @Autowired
    private LotDtoToLot toLot;

    @Autowired
    private TransfusionDtoToTransfusion toTransfusion;

    @Autowired
    private EndLotDtoToEndLot toEndLot;

    @Autowired
    private ReportService reportService;

    @Autowired
    private LotToLotDetailsDto toLotDetailsDto;

    @Autowired
    private LotToClosedLotDto toClosedLotDto;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "openLot")
    public ResponseEntity open(@RequestBody CreateLotDto lotDtoRequest) {

        Tank tank = tankService.findOneById(lotDtoRequest.getTankId());
        if (tank == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Lot lot = toLot.convert(lotDtoRequest, tank);
        Long tankId = lotService.findTankIdByUser(lot.getTank().getId());
        if (tankId != null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        lotService.save(lot);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/activeLots")
    public ResponseEntity<AllLotsDto> getAllLots() {

        List<Lot> lots = lotService.findAll().stream().filter(lot -> lot.getTank()!=null).collect(Collectors.toList());
       List<LotDetailsDto> lotDetailsDtos =  toLotDetailsDto.convert(lots);

        AllLotsDto allLotsDto = new AllLotsDto(lotDetailsDtos);
        return new ResponseEntity<>(allLotsDto, HttpStatus.OK);
    }

    @GetMapping("/closedLots")
    public ResponseEntity<AllClosedLotsDto> getClosedLots() {

        List<EndLot> lots = endLotService.findAll();
        List<ClosedLotDto> closedLotDtos =  toClosedLotDto.convert(lots);

        AllClosedLotsDto allClosedLotDtos = new AllClosedLotsDto(closedLotDtos);
        return new ResponseEntity<>(allClosedLotDtos, HttpStatus.OK);
    }

    @GetMapping("measurement/{lotId}")
    public ResponseEntity<List<MeasurementDto>> getMeasurements(@PathVariable("lotId") int id) {
        List<Fermentation> measurementsForLotId = storageSpentService.getMeasurementsForLotId(id);
        List<MeasurementDto> measurements = measurementsForLotId.stream().map(measurement -> new MeasurementDto((int) measurement.getLot().getId(),
                measurement.getSugar(),
                measurement.getTemperature(),
                measurement.getAlcohol(),
                measurement.getAcid(),
                measurement.getpH(),
                (measurement.getDate()))).collect(Collectors.toList());
        return new ResponseEntity<>(measurements, HttpStatus.OK);
    }

    @GetMapping("/details/{lotId}")
    public ResponseEntity<LotDetailsDto> getLotDetails(@PathVariable("lotId") Long id) {

        Lot lot = lotService.findOne(id);
        Tank tank = lot.getTank();
        LotDetailsDto lotDetailsDto = new LotDetailsDto((int) lot.getId(), lot.getLotName(), lot.getWineLabel(),
                lot.getWineColor().ordinal(), lot.getStartTime(),
                lot.getExpectedEndDate(),
                tank.getOrderNumber(),
                (int) lot.getUsedCapacity(),
                tank.getCapacity());
        return new ResponseEntity<>(lotDetailsDto, HttpStatus.OK);
    }

    @GetMapping("sulfur/{lotId}")
    public ResponseEntity<SulfurMeasurementDto> getSulfurMeasurementForLotId(@PathVariable("lotId") int id) {
        List<SulfurMeasurement> sulfurMeasurement = storageSpentService.getSulfurForLotId(id);
        if (sulfurMeasurement.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        SulfurMeasurementDto sulfurMeasurementDto = new SulfurMeasurementDto(sulfurMeasurement.get(0).getLot().getId(),
                sulfurMeasurement.get(0).getFso2(),
                sulfurMeasurement.get(0).getTso2(),
                sulfurMeasurement.get(0).getDate());
        return new ResponseEntity<>(sulfurMeasurementDto, HttpStatus.OK);
    }

    @GetMapping("/{tankId}")
    public ResponseEntity<LotDto> getLotByTankId(@PathVariable("tankId") Long tankId) {

        Lot lot = lotService.findOneByTankId(tankId);
        if (lot != null) {
            LotDto lotDto = new LotDto(lot.getId(), lot.getLotName());
            return new ResponseEntity<>(lotDto, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "addRawMaterialToLot")
    public ResponseEntity addRawMaterialToLot(@RequestBody RawMaterialEntryDto rawMaterialEntryDto) {

        RawMaterial rawMaterial = rawMaterialService.findOne(rawMaterialEntryDto.getRawMaterialId());
        Lot lot = lotService.findOne(rawMaterialEntryDto.getLotId());
        if (rawMaterial == null || lot == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        rawMaterial.setQuantity(rawMaterial.getQuantity() - rawMaterialEntryDto.getQuantity());
        rawMaterialService.save(rawMaterial);
        storageSpentService.save(new SpentMaterial(rawMaterialEntryDto.getQuantity(),
                rawMaterial, lot, rawMaterialEntryDto.getDate()));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "addEnologyToLot")
    public ResponseEntity addEnologyToLot(@RequestBody EnologyToLotDto enologyToLotdto) {
        Enology enology = enologyService.findOne(enologyToLotdto.getEnologyId());
        Lot lot = lotService.findOne(enologyToLotdto.getLotId());

        if (enology == null || lot == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        enology.setQuantity(enology.getQuantity() - enologyToLotdto.getQuantity());

        enologyService.save(enology);
        storageSpentService.save(new SpentEnology(enologyToLotdto.getQuantity(),
                enology, lot, enologyToLotdto.getDate(), enologyToLotdto.getComment()));

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "grapePressing")
    public ResponseEntity grapePress(@Validated @RequestBody GrapePressDto grapePressDto) {

        Lot lot = lotService.findOne(grapePressDto.getLotId());
        double totalCapacity = lot.getUsedCapacity() + grapePressDto.getStartingVolume();
        if (totalCapacity > lot.getTank().getCapacity()) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        lot.setUsedCapacity(lot.getUsedCapacity() + grapePressDto.getStartingVolume());

        lotService.save(lot);

        storageSpentService.save(new GrapePress(grapePressDto.getStartingVolume(), lot,
                grapePressDto.getDate(), grapePressDto.getComment()));

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "measurement")
    public ResponseEntity measurement(@RequestBody MeasurementDto measurementDto) {
        Lot lot = lotService.findOne(measurementDto.getLotId());
        Fermentation fermentation = new Fermentation(measurementDto.getSugar(),
                measurementDto.getTemperature(),
                measurementDto.getAlcohol(),
                measurementDto.getAcid(),
                measurementDto.getpH(),
                measurementDto.getDate(),
                lot);
        List<Fermentation> fermentations = storageSpentService.findFermentationdsByLotId(lot.getId());
        long index = fermentations.size();
        if (index <= 1) {
            storageSpentService.save(fermentation);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        Fermentation lastFermentation = fermentations.get(fermentations.size() - 1);
        long days = Duration.between(lastFermentation.getDate().atStartOfDay(), fermentation.getDate().atStartOfDay()).toDays();
        if (days > 1) {
            storageSpentService.save(fermentation);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }

        if (fermentation.getSugar() == null) {
            fermentation.setSugar(lastFermentation.getSugar());
        }
        if (fermentation.getTemperature() == null) {
            fermentation.setTemperature(lastFermentation.getTemperature());
        }
        if (fermentation.getAlcohol() == null) {
            fermentation.setAlcohol(lastFermentation.getAlcohol());
        }
        if (fermentation.getAcid() == null) {
            fermentation.setAcid(lastFermentation.getAcid());
        }
        if (fermentation.getpH() == null) {
            fermentation.setpH(lastFermentation.getpH());
        }
        storageSpentService.save(fermentation);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "sulfurMeasurement")
    public ResponseEntity sulfurMeasurement(@RequestBody SulfurMeasurementDto sulfurMeasurementDto) {

        Lot lot = lotService.findOne(sulfurMeasurementDto.getLotId());
        storageSpentService.save(new SulfurMeasurement(sulfurMeasurementDto.getTso2(),
                sulfurMeasurementDto.getFso2(),
                sulfurMeasurementDto.getDate(),
                lot));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "bottling")
    public ResponseEntity bottling(@RequestBody BottlingDto bottlingDto) {

        Lot lot = lotService.findOne(bottlingDto.getLotId());
        Packaging packaging = packagingService.findOneById(bottlingDto.getPackagingId());
        if (packaging == null) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Bottle bottle = packagingService.findBottleForPackagingId((long) bottlingDto.getPackagingId());
        if (bottle != null) {
            double newUsedCapacity = lot.getUsedCapacity() - (double) bottlingDto.getQuantity() * ((double) bottle.getVolume() / 1000);
            if (newUsedCapacity < 0) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            lot.setUsedCapacity(newUsedCapacity);
            lotService.save(lot);
        }
        packaging.setQuantity(packaging.getQuantity() - bottlingDto.getQuantity());
        packagingService.save(packaging);
        storageSpentService.save(new SpentPackaging(bottlingDto.getQuantity(), packaging, lot,
                bottlingDto.getDate()));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value = "/transfusion", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity transfusion(@RequestBody TransfusionDto transfusionDto) {
        Tank targetTank = tankService.findOneById(transfusionDto.getTargetTankId());
        if (targetTank == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Lot lot = lotService.findOne(transfusionDto.getLotId());
        double wasteAmount = transfusionDto.getWasteAmount();
        double usedCapacity = lot.getUsedCapacity();
        double newCapacity = usedCapacity - wasteAmount;
        transfusionDto.setWineAmount(newCapacity);

        transfusionService.save(toTransfusion.convert(transfusionDto));
        Lot updated = new Lot(lot.getId(), lot.getLotName(), lot.getStartTime(), lot.getExpectedEndDate(),
                lot.getWineLabel(), lot.getWineColor(), (int) newCapacity, targetTank);
        lotService.save(updated);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/endLot", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity endLot(@RequestBody EndLotDto endLotDto) {

        Lot lot = lotService.findOne(endLotDto.getLotId());
        if (lot.getTank() == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        EndLot endLot = toEndLot.convert(endLotDto);
        if (endLot == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        endLotService.save(endLot);

        try {
            reportService.createDocument(lot.getId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Lot updated = new Lot(lot.getId(), lot.getLotName(), lot.getStartTime(), lot.getExpectedEndDate(), lot.getWineLabel(),
                lot.getWineColor(), lot.getUsedCapacity(), null);
        lotService.save(updated);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
