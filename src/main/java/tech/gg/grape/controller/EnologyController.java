package tech.gg.grape.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tech.gg.grape.controller.dto.*;
import tech.gg.grape.repository.model.Enology;
import tech.gg.grape.repository.model.EnologyCategory;
import tech.gg.grape.repository.model.Unit;
import tech.gg.grape.service.service.EnologyService;
import tech.gg.grape.service.support.EnologyCategoryToEnologyCategoryDto;
import tech.gg.grape.service.support.EnologyDtoToEnology;
import tech.gg.grape.service.support.EnologyToEnologyDto;

import java.util.List;

@RestController
@RequestMapping(value = "/grappe/enology", produces = MediaType.APPLICATION_JSON_VALUE)
public class EnologyController {

    @Autowired
    EnologyService enologyService;

    @Autowired
    EnologyToEnologyDto toEnologyDto;

    @Autowired
    EnologyDtoToEnology toEnology;

    @Autowired
    EnologyCategoryToEnologyCategoryDto toEnologyCategoryDto;

    @GetMapping
    public ResponseEntity<List<EnologyDtoResponse>> getAll() {

        List<Enology> enologies = enologyService.findAll();
        return new ResponseEntity<>(toEnologyDto.convert(enologies), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EnologyDtoResponse> getOne(@PathVariable Long id) {

        Enology enology = enologyService.findOne(id);
        if (enology == null || enology.isDeleted()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(toEnologyDto.convert(enology), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<HttpStatus> create(@Validated @RequestBody EnologyDtoRequest enologyDto) {

        EnologyCategory enologyCategory = enologyService.findCategory(enologyDto.getCategory());

        if (enologyCategory == null || enologyDto.getUnit() >= Unit.values().length) {
            System.out.println(Unit.values().length);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Enology enology = enologyService.findByNameAndCategoryAndAmount(enologyDto.getName(), enologyDto.getCategory(),
                enologyDto.getAmount());
        if (enology != null && !enology.isDeleted()) {
            enologyService.save(new Enology(enology.getId(), enology.getName(),
                    enology.getQuantity() + enologyDto.getQuantity(),
                    enology.getCategory(), enology.getMoney()));
        } else if (enology != null && enology.isDeleted()) {
            enology.setQuantity(enologyDto.getQuantity());
            enology.setDeleted(false);
            enologyService.save(enology);
        } else {
            enologyService.save(toEnology.convert(enologyDto));
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/categories")
    public ResponseEntity<List<EnologyCategoryDto>> getAllEnologyCategory() {

        List<EnologyCategory> categories = enologyService.findAllCategories();
        return new ResponseEntity<>(toEnologyCategoryDto.convert(categories), HttpStatus.OK);
    }


    @PostMapping("/category")
    public ResponseEntity<EnologyCategory> createEnologyCategory(@RequestBody EnologyCategoryPostDto categoryDto) {
        EnologyCategory category = new EnologyCategory(categoryDto.getName());
        enologyService.saveCategory(category);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<EnologyDtoResponse> delete(@PathVariable long id) {

        Enology enology = enologyService.findOne(id);
        if (enology != null && !enology.isDeleted()) {
            enologyService.delete(enology);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PatchMapping(value = "/changeQuantity")
    public ResponseEntity<EnologyDtoResponse> changeQuantity(@RequestBody StorageItem storageItem) {

        if (storageItem.getEntityId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Enology enology = enologyService.findOne(storageItem.getEntityId());
        if (enology != null) {
            enologyService.changeQuantity(enology, storageItem.getQuantity());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
