package tech.gg.grape.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.gg.grape.controller.dto.TankDtoRequest;
import tech.gg.grape.controller.dto.TankDtoResponse;
import tech.gg.grape.service.service.LotService;
import tech.gg.grape.service.service.TankService;
import tech.gg.grape.repository.model.Lot;
import tech.gg.grape.repository.model.Tank;
import tech.gg.grape.service.support.TankDtoToTank;
import tech.gg.grape.service.support.TankToTankDto;

import java.util.List;

@RestController
@RequestMapping(value = "grappe/tank", produces = MediaType.APPLICATION_JSON_VALUE)
public class TankController {

    @Autowired
    TankService tankService;

    @Autowired
    TankToTankDto toTankDto;

    @Autowired
    TankDtoToTank toTank;

    @Autowired
    LotService lotService;

    @GetMapping
    public ResponseEntity<List<TankDtoResponse>> getAll() {

        List<Tank> tanks = tankService.findAll();
        return new ResponseEntity<>(toTankDto.convert(tanks), HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> create(@RequestBody TankDtoRequest tankDtoRequest) {

        List<Tank> tanks = tankService.findAll();
        for (Tank tank : tanks) {
            if (tank.getOrderNumber() == tankDtoRequest.getOrderNumber()) {
                return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
            }
        }
        tankService.save(toTank.convert(tankDtoRequest));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable long id) {
        Lot lot = lotService.findOneByTankId(id);
        if (lot != null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        Tank tank = tankService.findOneById(id);
        if (tank != null) {
            tankService.delete(tank);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
