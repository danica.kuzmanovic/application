#eyJhbGciOiJSUzI1NiIsImtpZCI6IjZmOGUxY2IxNTY0MTQ2M2M2ZGYwZjMzMzk0YjAzYzkyZmNjODg5YWMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZ3JhcHBlLWNkYjlhIiwiYXVkIjoiZ3JhcHBlLWNkYjlhIiwiYXV0aF90aW1lIjoxNjUzOTA3NTQyLCJ1c2VyX2lkIjoiMHV4QnpoRXB2UWhWT285OG1zS244Skg5bFJDMiIsInN1YiI6IjB1eEJ6aEVwdlFoVk9vOThtc0tuOEpIOWxSQzIiLCJpYXQiOjE2NTM5MjI3NDgsImV4cCI6MTY1MzkyNjM0OCwiZW1haWwiOiJwZXJsaWMuc3RlZmFuQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbInBlcmxpYy5zdGVmYW5AZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.JuR6264Fhxyc3veCMjJIlNUqky4fS_0tW_QpnPo24kBqV-dMP-EIBbLIsvZfQteQQ5vwXEA279Nv9h8bUib9rTu7CJuA1H4daPwsId_D_nhHfJB-9IgrMBitF-UA7oYHKMoz_fstX0EYg0amPIyBtFrdmYReKy2G8iXEO5vyP30_aPZDvRmIMCzB9o52sq6zFVeqZMEfbWtUpnUAz1EBRIBcVOTB-QpjLZOQeg12sa2QwQhrIivAGCP0p8kLJF32WjmqJHSmqUllFUzIP-NjgTbxU6GE1y3VYYfm_jmD42kev8ElwX9iKD-G7u-hytBTRN9t-sjxLcEPhZGne_kyqQ


USE wine;

INSERT INTO money (amount, unit)
VALUES (100, 0);

INSERT INTO enology_category (id, name) VALUES (0, 'Kategorija 1');
INSERT INTO enology_category (id, name) VALUES (2, 'Kategorija 2');
INSERT INTO enology_category (id, name) VALUES (3, 'Kategorija 3');


INSERT INTO enology (id, deleted, category_id, name, money_id, quantity)
VALUES (4, false, 1, 'Fermol', 1, 1);
INSERT INTO enology (id, deleted, category_id, name, money_id, quantity)
VALUES (5, false, 2, 'E224', 1, 1);
INSERT INTO enology (id, deleted, category_id, name, money_id, quantity)
VALUES (6, false,3, 'Superativante', 1, 1);
INSERT INTO enology (id, deleted, category_id, name, money_id, quantity)
VALUES (7,false, 3, 'Inoferm', 1, 1);
INSERT INTO enology (id, deleted, category_id, name, money_id, quantity)
VALUES (8, false,3, 'Vitalyest', 1, 1);
INSERT INTO enology (id, deleted, category_id, name, money_id, quantity)
VALUES (9, false, 3, 'Hrana za kvasac', 1, 1);



INSERT INTO raw_material (id, name, money_id, quantity, deleted)
VALUES (1, 'Chardonnay', 1, 1,  false);
INSERT INTO raw_material (id, name, money_id, quantity, deleted)
VALUES (2, 'Rajski Rizling', 1, 1,  false);
INSERT INTO raw_material (id, name, money_id, quantity, deleted)
VALUES (3, 'Muscat Petit Grain', 1, 1,  false);
INSERT INTO raw_material (id, name, money_id, quantity,  deleted)
VALUES (4, 'Merlot', 1, 1,  false);


#Long id, PackagingCategory category, String name, double quantity, Money money
INSERT INTO packaging (id, name, money_id, quantity, category,  deleted)
VALUES (1, 'Bordo bela', 1, 1, 0,  false);
INSERT INTO packaging (id, name, money_id, quantity, category,  deleted)
VALUES (2, 'Bordo tamna', 1, 1, 0,  false);
INSERT INTO packaging (id, name, money_id, quantity, category,  deleted)
VALUES (3, 'Burgundija', 1, 1, 0,  false);
INSERT INTO packaging (id, name, money_id, quantity, category,  deleted)
VALUES (4, 'Magnum Bordo', 1, 1, 0,  false);
INSERT INTO packaging (id, name, money_id, quantity, category,  deleted)
VALUES (5, 'Pampur DIAM 5', 1, 1, 1, false);
INSERT INTO packaging (id, name, money_id, quantity, category,  deleted)
VALUES (6, 'Krunski cep', 1, 1, 1,  false);
INSERT INTO packaging (id, name, money_id, quantity, category,  deleted)
VALUES (7, 'Screw cap', 1, 1, 1,  false);
INSERT INTO packaging (id, name, money_id, quantity, category,  deleted)
VALUES (8, 'Merlot', 1, 1, 2,  false);
INSERT INTO packaging (id, name, money_id, quantity, category, deleted)
VALUES (9, 'Roze kapica', 1, 1, 3, false);



INSERT INTO bottle(id, volume, volume_unit, packaging_id) VALUE (1, 1000, 0, 1);
INSERT INTO bottle(id, volume, volume_unit, packaging_id) VALUE (2, 1000, 0, 2);
INSERT INTO bottle(id, volume, volume_unit, packaging_id) VALUE (3, 1000, 0, 3);
INSERT INTO bottle(id, volume, volume_unit, packaging_id) VALUE (4, 1000, 0, 4);




INSERT INTO tank(id, capacity, order_number, tank_type)
VALUES (1, 1000, 1, 0);
INSERT INTO tank(id, capacity, order_number, tank_type)
VALUES (2, 1000, 2, 0);


INSERT INTO lot(id, expected_end_date, lot_name, start_time, used_capacity, wine_color, wine_label, tank_id)
VALUES (1, '2021-01-01', 'lotName', '2020-01-01', 0, 1, 'etiketa', 1);








