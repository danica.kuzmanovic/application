import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Enology } from './Components/Enology/Enology';
import { RawMaterial } from './Components/RawMaterial/RawMaterial';
import { Packaging } from './Components/Packaging/Packaging';
import { Tank } from './Components/Tank/Tank'
import { Navbar } from './Navbar';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { LotList } from './Components/Lot/LotList';
import { EditLot } from './Components/Lot/EditLot';
import { AddEnology } from './Components/Enology/AddEnology';
import { AddRawMaterial } from './Components/RawMaterial/AddRawMaterial';
import { AddPackaging } from './Components/Packaging/AddPackaging';
import { AddPackagingBottle } from './Components/Packaging/AddPackagingBottle';
import { AddTank } from './Components/Tank/AddTank';
import { useState } from 'react';
import { Menu } from './Menu';
import { OpenLot } from './Components/Lot/OpenLot';


function App() {
  const client = new QueryClient();

  const [menuToggle, setMenuToggle] = useState(false);

  return (
    <div className="App">
      <QueryClientProvider client={client}>
        <Router>
          <Navbar menuToggle={menuToggle} setMenuToggle={setMenuToggle} />
          <div className='app-content'>
            {menuToggle && <Menu />}
            <Routes>
              <Route path='/' element={<h1>Home</h1>} />
              <Route path='/enology' element={<Enology />} />
              <Route path='/enology/new' element={<AddEnology />} />
              <Route path='/rawMaterial' element={<RawMaterial />} />
              <Route path='/rawMaterial/new' element={<AddRawMaterial />} />
              <Route path='/packaging' element={<Packaging />} />
              <Route path='/packaging/newPackaging' element={<AddPackaging />} />
              <Route path='/packaging/newPackagingBottle' element={<AddPackagingBottle />} />
              <Route path='/tank' element={<Tank />} />
              <Route path='/tank/new' element={<AddTank />} />
              <Route path='/lot' element={<LotList />} />
              <Route path='/lot/edit/:id' element={<EditLot />} />
              <Route path='/openLot' element={<OpenLot />} />
            </Routes>
          </div>

        </Router>
      </QueryClientProvider>
    </div>
  );
}

export default App;
