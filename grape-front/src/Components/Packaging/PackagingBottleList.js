import UpdateQuantity from "../UpdateQuantity"
import { AiOutlineDelete } from 'react-icons/ai'
import { useMutation, useQueryClient } from "@tanstack/react-query"
import { deletePackaging } from '../../api/packaging'

export const PackagingBottleList = ({ bottleDtos }) => {

    const queryClient = useQueryClient()
    const deletePackagingMutation = useMutation(deletePackaging, {
        onSuccess: () => {
            queryClient.invalidateQueries(['packagings'])
        }
    });

    const handleDelete = (id) => {
        return deletePackagingMutation.mutate(parseInt(id))
    }

    return (
        <table>
            <thead>
                <tr>
                    <th style={{ textAlign: 'left' }}>Category</th>
                    <th style={{ textAlign: 'left' }}>Name</th>
                    <th>Quantity</th>
                    <th style={{ textAlign: 'right' }}>Volume</th>
                    <th style={{ textAlign: 'left' }}>Unit</th>
                    <th style={{ textAlign: 'right' }}>Amount</th>
                    <th style={{ textAlign: 'left' }}>Unit</th>
                    <th>Update Quantity</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {bottleDtos && bottleDtos.map((bottle) => {
                    return (
                        <tr key={bottle.id}>
                            <td style={{ textAlign: 'left' }}>{bottle.category}</td>
                            <td style={{ textAlign: 'left' }}>{bottle.name}</td>
                            <td>{bottle.quantity}</td>
                            <td style={{ textAlign: 'right' }}>{bottle.volume}</td>
                            <td style={{ textAlign: 'left' }}>{bottle.volumeUnit}</td>
                            <td style={{ textAlign: 'right' }}>{bottle.amount}</td>
                            <td style={{ textAlign: 'left' }}>{bottle.unit}</td>
                            <td>
                                <UpdateQuantity item={bottle} itemName='packaging' />
                            </td>
                            <td><AiOutlineDelete
                                className='delete'
                                role='button'
                                onClick={() => handleDelete(bottle.id)} /></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}