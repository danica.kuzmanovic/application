import Form from "react-bootstrap/Form"
import { Button, FloatingLabel } from 'react-bootstrap'
import { addPackaging } from '../../api/packaging'
import { useQueryClient, useMutation } from '@tanstack/react-query'
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'
import { useNavigate } from "react-router-dom";

export const AddPackaging = () => {

    const navigate = useNavigate()

    const schema = yup.object().shape({
        amount: yup.number().typeError("Amount must be 1 or more").min(1, "Amount must be 1 or more").required(),
        category: yup.number().typeError("You must choose category").required(),
        name: yup.string().required("Packaging name is required"),
        quantity: yup.number().typeError("Quantity must be 1 or more").min(1, "Quantity must be 1 or more").required(),
        unit: yup.number().typeError("You must choose unit").integer().oneOf([0]).required()
    })

    const { register,
        handleSubmit,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });

    const queryClient = useQueryClient();

    const addPackagingMutation = useMutation(addPackaging, {
        onSuccess: () => {
            queryClient.invalidateQueries(['packagings'])
        }
    })

    const onSubmit = (data) => {
        addPackagingMutation.mutate(data);
        navigate('/packaging')
    }

    return (
        <div className="addItemForm">
            <h3>New Packaging</h3>
            <Form noValidate onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Name:">
                        <Form.Control
                            placeholder="Packaging Name"
                            type='text'
                            {...register('name')}
                            isInvalid={errors.name}
                        />
                    </FloatingLabel>
                    <Form.Control.Feedback type="invalid">
                        {errors.name && errors.name.message}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Category:">
                        <Form.Control
                            as='select'
                            {...register('category')}
                            isInvalid={errors.category}
                        >
                            <option>choose category</option>
                            <option value='1'>Bottle cap</option>
                            <option value='2'>Label</option>
                            <option value='3'>Decorative cap</option>
                        </Form.Control>
                    </FloatingLabel>
                    <Form.Control.Feedback type="invalid">
                        {errors.category && errors.category.message}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Quantity:">
                        <Form.Control
                            type='number'
                            {...register('quantity')}
                            isInvalid={errors.quantity}
                        />
                    </FloatingLabel>
                    <Form.Control.Feedback type="invalid">
                        {errors.quantity && errors.quantity.message}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Amount:">
                        <Form.Control
                            type='number'
                            {...register('amount')}
                            isInvalid={errors.amount}
                        />
                    </FloatingLabel>
                    <Form.Control.Feedback type="invalid">
                        {errors.amount && errors.amount.message}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Unit:">
                        <Form.Select
                            {...register('unit')}
                            isInvalid={errors.unit}
                        >
                            <option>Choose unit</option>
                            <option value='0'>Din</option>
                        </Form.Select>
                    </FloatingLabel>
                    <Form.Control.Feedback type="invalid">
                        {errors.amount && errors.amount.message}
                    </Form.Control.Feedback>
                </Form.Group>
                <Button type='submit'>Add</Button>
            </Form>
        </div >
    )
}