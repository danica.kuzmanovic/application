import { useQuery } from "@tanstack/react-query"
import { getAllPackaging } from "../../api/packaging"
import { PackagingBottleList } from "./PackagingBottleList";
import { PackagingList } from "./PackagingList";

export const Packaging = () => {

    const {
        data: packagings = [],
        isLoading,
        isError
    } = useQuery(['packagings'], getAllPackaging);

    const bottleDtos = packagings.bottleDtos && packagings.bottleDtos.map((bottle) => {
        const packaging = packagings.packagingDtos && packagings.packagingDtos.filter((packaging) => {
            return (packaging.id === bottle.packagingId)
        })
        return {
            id: packaging[0].id,
            category: packaging[0].categoryName === 0 && "Bottle",
            name: packaging[0].name,
            quantity: packaging[0].quantity,
            volume: bottle.volume,
            volumeUnit: bottle.volumeUnit === 0 && 'ml',
            amount: packaging[0].amount,
            unit: packaging[0].unit === 0 && 'rsd'
        }
    })

    const packagingDtos = (packagings.packagingDtos && packagings.packagingDtos.filter((packaging) => {
        return (packaging.categoryName !== 0)
    }))?.map(packagingDto => {
        return {
            id: packagingDto.id,
            category: (packagingDto.categoryName === 1 && 'Bottle cap')
                || (packagingDto.categoryName === 2 && 'Label')
                || (packagingDto.categoryName === 3 && 'Decorative cap'),
            name: packagingDto.name,
            quantity: packagingDto.quantity,
            amount: packagingDto.amount,
            unit: packagingDto.unit === 0 && 'rsd'
        }
    })

    console.log(packagingDtos)

    let content
    if (isLoading) {
        content = <h1>isLoading</h1>
    }
    if (isError) {
        content = <h1>{Error.message}</h1>
    }
    return (
        <div className="table-entity">
            {packagingDtos && packagingDtos.length ? <PackagingList packagingDtos={packagingDtos} />
                : "Packaging list is empty"}
            <br />
            {bottleDtos && bottleDtos.length ? <PackagingBottleList bottleDtos={bottleDtos} />
                : "Packaging list is empty"}
        </div>
    )
}