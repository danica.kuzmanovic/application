import UpdateQuantity from "../UpdateQuantity"
import { AiOutlineDelete } from 'react-icons/ai'
import { useMutation, useQueryClient } from "@tanstack/react-query"
import { deletePackaging } from '../../api/packaging'

export const PackagingList = ({ packagingDtos }) => {

    const queryClient = useQueryClient()
    const deletePackagingMutation = useMutation(deletePackaging, {
        onSuccess: () => {
            queryClient.invalidateQueries(['packagings'])
        }
    });

    const handleDelete = (id) => {
        return deletePackagingMutation.mutate(parseInt(id))
    }

    return (
        <table>
            <thead>
                <tr>
                    <th style={{ textAlign: 'left' }}>Category</th>
                    <th style={{ textAlign: 'left' }}>Name</th>
                    <th>Quantity</th>
                    <th style={{ textAlign: 'right' }}>Amount</th>
                    <th style={{ textAlign: 'left' }}>Unit</th>
                    <th>Update Quantity</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {packagingDtos && packagingDtos.map(packagingDto => {
                    return (
                        <tr key={packagingDto.id}>
                            <td style={{ textAlign: 'left' }}>{packagingDto.category}</td>
                            <td style={{ textAlign: 'left' }}>{packagingDto.name}</td>
                            <td>{packagingDto.quantity}</td>
                            <td style={{ textAlign: 'right' }}>{packagingDto.amount}</td>
                            <td style={{ textAlign: 'left' }}>{packagingDto.unit}</td>
                            <td>
                                <UpdateQuantity item={packagingDto} itemName='packaging' />
                            </td>
                            <td><AiOutlineDelete
                                className='delete'
                                role='button'
                                onClick={() => handleDelete(packagingDto.id)} /></td>
                        </tr>
                    )
                })}

            </tbody>
        </table>
    )
}