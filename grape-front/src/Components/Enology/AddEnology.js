import { IoIosAdd } from 'react-icons/io'
import Form from "react-bootstrap/Form"
import { Button, InputGroup, FloatingLabel } from 'react-bootstrap'
import { addEnology, getAllCategories, addEnologyCategory } from '../../api/enology'
import { useQuery, useQueryClient, useMutation } from '@tanstack/react-query'
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'


export const AddEnology = () => {

    const navigate = useNavigate();

    const [category, setCategory] = useState({
        name: ""
    });

    const schema = yup.object().shape({
        amount: yup.number().typeError("Amount must be 1 or more").min(1, "Amount must be 1 or more").required(),
        category: yup.number().typeError("You must choose category").required(),
        name: yup.string().required("Enology name is required"),
        quantity: yup.number().typeError("Quantity must be 1 or more").min(1, "Quantity must be 1 or more").required(),
        unit: yup.number().typeError("You must choose unit").integer().oneOf([0]).required()
    })

    const { register,
        handleSubmit,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });

    const queryClient = useQueryClient();

    const {
        data: categories = []
    } = useQuery(['categories'], getAllCategories)

    const addEnologyMutation = useMutation(addEnology, {
        onSuccess: () => {
            queryClient.invalidateQueries(['enologies'])
        }
    })

    const addCategoryMutation = useMutation(addEnologyCategory, {
        onSuccess: () => {
            queryClient.invalidateQueries(['categories'])
        }
    })

    const onSubmit = (data) => {
        addEnologyMutation.mutate(data);
        navigate("/enology")
        
    }

    const handleOnChangeCategory = (event) => {
        const { name, value } = event.target;
        setCategory({
            [name]: value
        })
    }


    const handleAddCategory = (category) => {
        addCategoryMutation.mutate(category)
    }

    return (
        <div className="addItemForm">
            <h3>New enology</h3>
            <Form noValidate onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Name:">
                        <Form.Control
                            placeholder="Enology Name"
                            type='text'
                            {...register('name')}
                            isInvalid={errors.name}
                        />
                        <Form.Control.Feedback type="invalid">
                            {errors.name && errors.name.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Category:">
                        <Form.Control
                            as='select'
                            name='category'
                            {...register('category')}
                            isInvalid={errors.category}
                        >
                            <option>choose category</option>
                            {categories.map((category) => {
                                return (<option value={category.id} key={category.id}> {category.name}</option>
                                )
                            })}
                        </Form.Control>
                        <Form.Control.Feedback type="invalid">
                            {errors.category && errors.category.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                    <InputGroup className='mt-3 mb-1'>
                        <Form.Control
                            type='text'
                            name='name'
                            placeholder="Add New Category"
                            onChange={handleOnChangeCategory}
                        />
                        <IoIosAdd
                            role='button'
                            className='addCategory'
                            onClick={() => handleAddCategory(category)}
                        />
                    </InputGroup>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Quantity:">
                        <Form.Control
                            type='number'
                            name='quantity'
                            {...register('quantity')}
                            isInvalid={errors.quantity}
                        />
                        <Form.Control.Feedback type="invalid">
                            {errors.quantity && errors.quantity.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Amount:">
                        <Form.Control
                            type='number'
                            name='amount'
                            {...register('amount')}
                            isInvalid={errors.amount}
                        />
                        <Form.Control.Feedback type="invalid">
                            {errors.amount && errors.amount.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="unit:">
                        <Form.Select
                            {...register('unit')}
                            isInvalid={errors.unit}
                        >
                            <option>Choose unit</option>
                            <option value='0'>Din</option>
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">
                            {errors.amount && errors.amount.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                </Form.Group>
                <Button type='submit'>Add</Button>
            </Form>
            
        </div >
    )
}