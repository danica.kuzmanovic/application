import { AiOutlineDelete } from 'react-icons/ai'
import UpdateQuantity from '../UpdateQuantity'
import { useQueryClient, useMutation } from '@tanstack/react-query';
import { deleteEnology } from '../../api/enology'

export const EnologyList = ({ enologies }) => {

    const queryClient = useQueryClient();

    const deleteEnologyMutation = useMutation(deleteEnology, {
        onSuccess: () =>
            queryClient.invalidateQueries(['enologies'])
    })

    const handleDelete = (id) => {
        deleteEnologyMutation.mutate(parseInt(id))
    }

    return (
        <table>
            <thead>
                <tr>
                    <th style={{ textAlign: 'left' }}>Name</th>
                    <th style={{ textAlign: 'left' }}>Category</th>
                    <th>Quantity</th>
                    <th style={{ textAlign: 'right' }}>Amount</th>
                    <th style={{ textAlign: 'left' }}>Unit</th>
                    <th>Update Quantity</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {enologies && enologies.map((enology) => {
                    return (
                        <tr key={enology.id}
                        >
                            <td style={{ textAlign: 'left' }}>{enology.name}</td>
                            <td style={{ textAlign: 'left' }}>{enology.category}</td>
                            <td>{enology.quantity}</td>
                            <td
                                style={{ textAlign: 'right' }}
                            >{enology.amount}</td>
                            <td style={{ textAlign: 'left' }}>{enology.unit === 0 && "rsd"}</td>
                            <td>
                                <UpdateQuantity item={enology} itemName='enology' />
                            </td>
                            <td><AiOutlineDelete
                                className='delete'
                                role='button'
                                onClick={() => handleDelete(enology.id)} /></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}