import { useQuery } from '@tanstack/react-query';
import { getAllEnology} from '../../api/enology'
import { EnologyList } from './EnologyList';
import '../../css/table.css'

export const Enology = () => {

    //prvi argument je array koji sadrzi unikatni id za ovaj query
    //drugi argument je funkcija koja radi sa podacima (axios)
    const { data: enologies = [],
        isLoading,
        isError } = useQuery(['enologies'], getAllEnology)

    let content
    if (isLoading) {
        content = <p>is loading</p>
    } else if (isError) {
        content = <p>{Error.message}</p>
    }


    return (
        <div className='table-entity'>
            <h1>{content}</h1>
            {enologies.length ? 
            <EnologyList enologies={enologies} />
            : 'Enology list is empty'}
        </div>
    )
}