import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { CgArrowsExchangeAlt } from 'react-icons/cg'
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { updateEnologyQuantity } from '../api/enology'
import { updatePackagingQuantity } from '../api/packaging'
import { updateRawMaterialQuantity } from '../api/rawMaterial';

function UpdateQuantity({ item, itemName }) {
    const [show, setShow] = useState(false);
    const [newQuantity, setNewQuantity] = useState(0)

    const client = useQueryClient()

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const updateEnologyQuantityMutation = useMutation(updateEnologyQuantity, {
        onSuccess: () => {
            client.invalidateQueries(['enologies'])
        }
    })

    const updatePackagingQuantityMutation = useMutation(updatePackagingQuantity, {
        onSuccess: () => {
            client.invalidateQueries(['packagings'])
        }
    })
    const updateRawMaterialQuantityMutation = useMutation(updateRawMaterialQuantity, {
        onSuccess: () => {
            client.invalidateQueries(['rawMaterials'])
        }
    })

    const handleUpdateQuantity = (id, name, quantity) => {
        const itemDto = {
            'entityId': id,
            'quantity': parseInt(quantity)
        }
        setNewQuantity(0)
        setShow(false)
        if (name === 'enology') {
            return updateEnologyQuantityMutation.mutate(itemDto)
        }
        if (name === 'packaging') {
            return updatePackagingQuantityMutation.mutate(itemDto)
        }
        if (name === 'rawMaterial') {
            return updateRawMaterialQuantityMutation.mutate(itemDto)
        }
    }
    return (
        <>
            <button
                className='btn btn-outline-dark btn-sm'
                onClick={handleShow}
                style={{ padding: 5 }}
            >
                <CgArrowsExchangeAlt
                    type='button'
                /> </button>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>{item.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <label>Add/Remove</label>
                    <input
                        type='number'
                        value={newQuantity}
                        onChange={(event) => setNewQuantity(event.target.value)}
                    />
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button
                        variant="primary"
                        onClick={() => handleUpdateQuantity(item.id, itemName, newQuantity)}

                    >Update Quantity</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default UpdateQuantity;