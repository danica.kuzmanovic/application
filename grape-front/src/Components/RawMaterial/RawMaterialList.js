import { AiOutlineDelete } from 'react-icons/ai'
import UpdateQuantity from '../UpdateQuantity'
import { useQueryClient, useMutation } from '@tanstack/react-query'
import { deleteRawMaterial } from '../../api/rawMaterial'

export const RawMaterialList = ({ rawMaterials }) => {


    const queryClient = useQueryClient();

    const deleteRawMaterialMutation = useMutation(deleteRawMaterial, {
        onSuccess: () =>
            queryClient.invalidateQueries(['rawMaterials'])
    })

    const handleDelete = (id) => {
        deleteRawMaterialMutation.mutate(parseInt(id))
    }

    return (
        <table>
            <thead>
                <tr>
                    <th style={{ textAlign: 'left' }}>Name</th>
                    <th>Quantity</th>
                    <th style={{ textAlign: 'right' }}>Amount</th>
                    <th style={{ textAlign: 'left' }}>Unit</th>
                    <th>Update Quantity</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {rawMaterials && rawMaterials.map((rawMaterial) => {
                    return (
                        <tr key={rawMaterial.id}
                        >
                            <td style={{ textAlign: 'left' }}>{rawMaterial.name}</td>
                            <td>{rawMaterial.quantity}</td>
                            <td
                                style={{ textAlign: 'right' }}
                            >{rawMaterial.amount}</td>
                            <td style={{ textAlign: 'left' }}>{rawMaterial.unit === 0 && "rsd"}</td>
                            <td
                                style={{ textAlign: 'center' }}>
                                <UpdateQuantity item={rawMaterial} itemName='rawMaterial' />
                            </td>
                            <td><AiOutlineDelete
                                className='delete'
                                role='button'
                                onClick={() => handleDelete(rawMaterial.id)} /></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )

}