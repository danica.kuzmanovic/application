import Form from "react-bootstrap/Form"
import { Button, FloatingLabel } from 'react-bootstrap'
import { addRawMaterial } from '../../api/rawMaterial'
import { useQueryClient, useMutation } from '@tanstack/react-query'
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'
import { useNavigate } from "react-router-dom";


export const AddRawMaterial = () => {

    const navigate = useNavigate()

    const schema = yup.object().shape({
        amount: yup.number().typeError("Amount must be 1 or more").min(1, "Amount must be 1 or more").required(),
        name: yup.string().required("RawMaterial name is required"),
        quantity: yup.number().typeError("Quantity must be 1 or more").min(1, "Quantity must be 1 or more").required(),
        unit: yup.number().typeError("You must choose unit").integer().oneOf([0]).required()
    })

    const { register,
        handleSubmit,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });

    const queryClient = useQueryClient();

    const addRawMaterialMutation = useMutation(addRawMaterial, {
        onSuccess: () => {
            queryClient.invalidateQueries(['rawMaterials'])
        }
    })

    const onSubmit = (data) => {
        console.log(data)
        addRawMaterialMutation.mutate(data);
        navigate('/rawMaterial')
    }

    return (
        <div className="addItemForm ">
            <h3>New RawMaterial</h3>
            <Form noValidate onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Name:">
                        <Form.Control
                            placeholder="RawMaterial Name"
                            type='text'
                            {...register('name')}
                            isInvalid={errors.name}
                        />
                        <Form.Control.Feedback type="invalid">
                            {errors.name && errors.name.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Quantity:">
                        <Form.Control
                            type='number'
                            name='quantity'
                            {...register('quantity')}
                            isInvalid={errors.quantity}
                        />
                        <Form.Control.Feedback type="invalid">
                            {errors.quantity && errors.quantity.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Amount:">
                        <Form.Control
                            type='number'
                            name='amount'
                            {...register('amount')}
                            isInvalid={errors.amount}
                        />
                        <Form.Control.Feedback type="invalid">
                            {errors.amount && errors.amount.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="unit:">
                        <Form.Select
                            {...register('unit')}
                            isInvalid={errors.unit}
                        >
                            <option>Choose unit</option>
                            <option value='0'>Din</option>
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">
                            {errors.amount && errors.amount.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                </Form.Group>
                <Button type='submit'>Add</Button>
            </Form>
        </div >
    )
}