import { useQuery } from '@tanstack/react-query';
import { getAllRawMaterials } from '../../api/rawMaterial'
import { RawMaterialList } from './RawMaterialList';

export const RawMaterial = () => {
    const { data: rawMaterials = [],
        isLoading,
        isError } = useQuery(['rawMaterials'], getAllRawMaterials)

    let content
    if (isLoading) {
        content = <p>is loading</p>
    } else if (isError) {
        content = <p>{Error.message}</p>
    }

    return (
        <div className="table-entity">
            <h1>{content}</h1>
            {rawMaterials.length ?
                <RawMaterialList rawMaterials={rawMaterials} />
                : "Raw material list is empty"}
        </div>
    )
}