import { Form, FloatingLabel, Button } from 'react-bootstrap'
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { addTank } from '../../api/tank';
import { useNavigate } from 'react-router-dom';


export const AddTank = () => {

    const navigate = useNavigate()
    const schema = yup.object().shape(
        {
            capacity: yup.number().typeError("Capacity must be 1 or more").positive("Capacity must be 1 or more"),
            orderNumber: yup.number().typeError("Tank number must be a unique number").min(1).required(),
            tankType: yup.number().typeError("Yoy must choose type").oneOf([0, 1])
        }
    )

    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(schema)
    })

    const queryClient = useQueryClient();

    const addTankMutation = useMutation(addTank, {
        onSuccess: () => {
            queryClient.invalidateQueries(['tanks'])
        }
    })

    const onSubmit = (data) => {
        console.log(data)
        addTankMutation.mutate(data)
        navigate('/tank')
    }

    return (
        <div className="addItemForm">
            <h3>New Tank</h3>
            <Form noValidate onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Tank Number:">
                        <Form.Control
                            type='number'
                            {...register('orderNumber')}
                            isInvalid={errors.orderNumber}
                        />
                        <Form.Control.Feedback type="invalid">
                            {errors.orderNumber && errors.orderNumber.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Capacity (l): ">
                        <Form.Control
                            type='number'
                            {...register('capacity')}
                            isInvalid={errors.capacity}
                        />
                        <Form.Control.Feedback type="invalid">
                            {errors.capacity && errors.capacity.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Type:">
                        <Form.Select
                            {...register('tankType')}
                            isInvalid={errors.tankType}
                        >
                            <option>Choose type</option>
                            <option value='0'>Tank</option>
                            <option value='1'>Barrel</option>
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">
                            {errors.tankType && errors.tankType.message}
                        </Form.Control.Feedback>
                    </FloatingLabel>
                </Form.Group>
                <Button type='submit'>Add</Button>
            </Form>
        </div >
    )
}