import { useQuery } from "@tanstack/react-query"
import { getAllTanks } from "../../api/tank";
import { Tanks } from "./Tanks";

export const Tank = () => {

    const {
        data: tanks = []
    } = useQuery(['tanks'], getAllTanks);

    return (
        <div className="tanks">
            {tanks.length ? tanks.map(tank => {
                return <Tanks key={tank.id} tank={tank} />
            }) : "Tank list is empty"
            }

        </div>
    )
}