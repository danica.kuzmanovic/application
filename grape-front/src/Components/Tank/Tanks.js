import { useMutation, useQueryClient } from '@tanstack/react-query'
import { AiOutlineDelete } from 'react-icons/ai'
import { deleteTank } from '../../api/tank';
import '../../css/tank.css'

export const Tanks = ({ tank }) => {

    const queryClient = useQueryClient();

    const deleteTankMutation = useMutation(deleteTank, {
        onSuccess: () => {
            queryClient.invalidateQueries('tanks')
        }
    })

    const handleDelete = (tank) => {
        deleteTankMutation.mutate(tank.id)
    }

    return (
        <div className="tank" key={tank.id}>
            {tank.tankType === 0 && <img className='tank-img' src="tank.png" />}
            {tank.tankType === 1 && <img className='tank-img' src="barrel.png" />}
            <label>{tank.orderNumber} </label>
            {tank.tankDetailsDto !== null ?
                <label>used capacity {tank.tankDetailsDto.usedCapacity} /
                    {tank.capacity} L
                </label> :
                <label>
                    {tank.capacity} L
                </label>}
            {tank.tankDetailsDto === null &&
                <AiOutlineDelete
                    className='delete'
                    role='button'
                    onClick={() => handleDelete(tank)}
                />}
        </div>
    )
}