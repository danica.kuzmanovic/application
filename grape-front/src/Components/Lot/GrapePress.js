import { FloatingLabel, Form } from 'react-bootstrap'
import { lotGrapePress } from '../../api/lot';
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import { useMutation, useQueryClient } from "@tanstack/react-query"
import { useNavigate } from 'react-router-dom';

export const GrapePress = ({ lot }) => {
    const navigate = useNavigate()

    console.log(lot)
    const schema = yup.object().shape({
        startingVolume: yup.number().max(lot.maxCapacity).required(),
        date: yup.string().required(),
        comment: yup.string().default("")

    })
    const { register,
        handleSubmit,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });


    const queryClient = useQueryClient()

    const grapePressMutation = useMutation(lotGrapePress, {
        onSuccess: () => {
            queryClient.invalidateQueries(['lots'])
        }
    })
    const handleGrapePress = (grapePressDto) => {

        console.log(grapePressDto)
        grapePressMutation.mutate(grapePressDto)
    }


    const onSubmit = (data) => {
        const grapePressDto = {
            date: data.date,
            lotId: lot.lotId,
            comment: data.comment,
            startingVolume: data.startingVolume
        }
        handleGrapePress(grapePressDto)
        navigate('/lot')
    }

    return (
        <div>
            <Form className="addToLot-form" noValidate onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Starting volume:">
                        <Form.Control
                            type='number'
                            name='startingVolume'
                            {...register('startingVolume')}
                            isInvalid={errors.startingVolume}
                        />
                    </FloatingLabel>
                    {errors.startingVolume && <label>
                        Maximum starting volume is tank capacity ({lot.maxCapacity}l)
                    </label>

                    }
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Date:">
                        <Form.Control
                            type='date'
                            name='amount'
                            {...register('date')}
                            isInvalid={errors.date}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3" >
                    <FloatingLabel controlId="floatingInputGrid" label="Comment:">
                        <Form.Control
                            type='textarea'
                            name='comment'
                            {...register('comment')}
                            isInvalid={errors.comment}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <button type='submit'>Add</button>
                </Form.Group>

            </Form>
        </div>
    )
}