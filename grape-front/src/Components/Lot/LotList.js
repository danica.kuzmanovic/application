import { Lot } from "./Lot"
import { useQuery } from "@tanstack/react-query"
import { getActiveLots, getClosedLots } from "../../api/lot"
import { ClosedLots } from "./ClosedLots"

export const LotList = () => {

    const { data: lots = [] } = useQuery(['lots'], getActiveLots)
    const { data: closedLots = [] } = useQuery(['closedLots'], getClosedLots)

    console.log(lots)

    return (
        <div className='table-entity'>
          {lots.length > 0 && <Lot lots={lots}/>}
         {closedLots.length > 0 && <ClosedLots closedLots={closedLots}/>}
        </div>
    )
}