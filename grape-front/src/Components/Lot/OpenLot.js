import Form from "react-bootstrap/Form"
import { Button, InputGroup, FloatingLabel } from 'react-bootstrap'
import { useQuery, useQueryClient, useMutation } from '@tanstack/react-query'
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'
import { useNavigate } from 'react-router-dom'
import { openLot } from '../../api/lot'
import { getAllTanks } from "../../api/tank";


export const OpenLot = () => {

    const navigate = useNavigate();


    const schema = yup.object().shape({
        expectedEndDate: yup.string().required(),
        lotName: yup.string().required(),
        startDate: yup.string().required(),
        tankId: yup.number().required(),
        wineColor: yup.number().required(),
        wineLabel: yup.string().required()
    })

    const { register,
        handleSubmit,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });

        const {
            data: tanks = []
        } = useQuery(['tanks'], getAllTanks)


    const queryClient = useQueryClient();

    const openLotMutation = useMutation(openLot, {
        onSuccess: () => {
            queryClient.invalidateQueries(['lots'])
        }
    })

    const onSubmit = (data) => {
        console.log(data)
        openLotMutation.mutate(data);
        navigate("/lot")
    }

    return (
        <div className="addItemForm">
            <h3>Open Lot</h3>
            <Form noValidate onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Name:">
                        <Form.Control
                            type='text'
                            {...register('lotName')}
                            isInvalid={errors.lotName}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Wine label:">
                        <Form.Control
                            type='text'
                            {...register('wineLabel')}
                            isInvalid={errors.wineLabel}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Tank:">
                        <Form.Select
                            {...register('tankId')}
                            isInvalid={errors.tankId}
                        >
                            <option>Choose tank</option>
                            {tanks.map((tank) => {
                                return (<option value={parseInt(tank.id)} key={tank.id}> {tank.orderNumber}</option>
                                )
                            })}
                        </Form.Select>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="mb-3">
                    <FloatingLabel controlId="floatingInputGrid" label="wineColor:">
                        <Form.Select
                            {...register('wineColor')}
                            isInvalid={errors.wineColor}
                        >
                            <option>Choose wine Color</option>
                            <option value={0}>Red</option>
                            <option value={1}>White</option>
                            <option value={2}>Rose</option>
                            <option value={3}>Orange</option>
                        </Form.Select>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Start date:">
                        <Form.Control
                            type='date'
                            name='startDate'
                            {...register('startDate')}
                            isInvalid={errors.startDate}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Expected end date:">
                        <Form.Control
                            type='date'
                            name='expectedEndDate'
                            {...register('expectedEndDate')}
                            isInvalid={errors.expectedEndDate}
                        />
                    </FloatingLabel>
                </Form.Group>
                    <button type='submit'>Add</button>
            </Form>

        </div >
    )
}