import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query"
import { getAllEnology } from "../../api/enology"
import { addEnologyToLot } from '../../api/lot'
import { FloatingLabel, Form } from 'react-bootstrap'
import { yupResolver } from '@hookform/resolvers/yup'
import *  as yup from 'yup';
import { useForm } from 'react-hook-form';
import { useNavigate } from "react-router-dom"

export const AddEnologyToLot = ({ lot }) => {

    const navigate = useNavigate()
    const { data: enologies = [] } = useQuery(['enologies'], getAllEnology)

    const schema = yup.object().shape({
        enology: yup.number().required(),
        quantity: yup.number().typeError("").positive("").required(),
        date: yup.string().required(),
        comment: yup.string().default("")
    })

    const { register,
        handleSubmit,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });

    const queryClient = useQueryClient()

    const addEnologyToLotMutation = useMutation(addEnologyToLot, {
        onSuccess: () => {
            queryClient.invalidateQueries(['enologies'])
        }
    })
    // const handleAddEnology = (addEnologyDto) => {
    //     addEnologyToLotMutation.mutate(addEnologyDto)
    // }

    const onSubmit = (data) => {
        const addEnologyDto = {
            comment: data.comment,
            date: data.date,
            enologyId: data.enology,
            lotId: lot.lotId,
            quantity: data.quantity
        }
        console.log(addEnologyDto)
        addEnologyToLotMutation.mutate(addEnologyDto)
        navigate('/lot')
    }

    return (
        <div>
            <Form className="addToLot-form" noValidate onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Enology:">
                        <Form.Select
                            {...register('enology')}
                            isInvalid={errors.enology}
                        >
                            <option>choose enology</option>
                            {enologies.map((enology) => {
                                return (
                                    <option
                                        value={enology.id}
                                        key={enology.id}> {enology.name}</option>
                                )
                            })}
                        </Form.Select>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Quantity:">
                        <Form.Control
                            type='number'
                            name='quantity'
                            {...register('quantity')}
                            isInvalid={errors.quantity}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Date:">
                        <Form.Control
                            type='date'
                            name='amount'
                            {...register('date')}
                            isInvalid={errors.date}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3" >
                    <FloatingLabel controlId="floatingInputGrid" label="Comment:">
                        <Form.Control
                            type='textarea'
                            name='comment'
                            {...register('comment')}
                            isInvalid={errors.comment}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <button type='submit'>Add</button>
                </Form.Group>
            </Form>
        </div>
    )
}