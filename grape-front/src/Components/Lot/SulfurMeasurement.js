import { FloatingLabel, Form } from 'react-bootstrap'
import { lotSulfurMeasurement } from '../../api/lot';
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import { useMutation, useQueryClient } from "@tanstack/react-query"
import { useNavigate } from 'react-router-dom';

export const SulfurMeasurement = ({ lot }) => {

    const navigate = useNavigate()

    const schema = yup.object().shape({
        fso2: yup.number().required(),
        tso2: yup.number().required(),
        date: yup.string().required()
    })
    const { register,
        handleSubmit,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });


    const queryClient = useQueryClient()

    const sulfurMeasurementMutation = useMutation(lotSulfurMeasurement, {
        onSuccess: () => {
            queryClient.invalidateQueries(['lots'])
        }
    })
    const handleSulfurMeasurement = (sulfurMeasurementDto) => {

        console.log(sulfurMeasurementDto)
        sulfurMeasurementMutation.mutate(sulfurMeasurementDto)
    }

    const onSubmit = (data) => {

        const sulfurMeasurementDto = {
            date: data.date,
            lotId: lot.lotId,
            tso2: data.tso2,
            fso2: data.fso2
        }
        console.log(sulfurMeasurementDto)

        handleSulfurMeasurement(sulfurMeasurementDto)
        navigate('/lot')
    }

    return (
        <div>
            <Form className="addToLot-form" noValidate onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="tso2:">
                        <Form.Control
                            type='number'
                            name='tso2'
                            {...register('tso2')}
                            isInvalid={errors.tso2}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="fso2:">
                        <Form.Control
                            type='number'
                            name='fso2'
                            {...register('fso2')}
                            isInvalid={errors.fso2}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Date:">
                        <Form.Control
                            type='date'
                            name='amount'
                            {...register('date')}
                            isInvalid={errors.date}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <button type='submit'>Add</button>
                </Form.Group>
            </Form>
        </div>
    )
}