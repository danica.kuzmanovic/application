import { useLocation } from "react-router-dom"
import { AddEnologyToLot } from "../Lot/AddEnologyToLot"
import { AddRawMaterialToLot } from "./AddRawMaterialToLot"
import { Measurement } from "./Measurement"
import { GrapePress } from "./GrapePress"
import { SulfurMeasurement } from "./SulfurMeasurement"
import { Transfusion } from "./Transfusion"
import '../../css/lotEdit.css'
import { useState } from "react"
import { AddBottling } from "./AddBottling"

export const EditLot = () => {

    const location = useLocation()
    const lot = location.state

    const [operation, setOperation] = useState({
        addEnology: false,
        addRawMaterial: false,
        measurement: false,
        grapePress: false,
        sulfurMeasurement: false,
        transfusion: false,
        bottling: false
    })

    const handleClick = (event) => {
        const { name } = event.target
        setOperation({
            addEnology: false,
            addRawMaterial: false,
            measurement: false,
            grapePress: false,
            sulfurMeasurement: false,
            transfusion: false,
            bottling: false,
            [name]: !operation[name]
        })
    }

    return (
        <div className="editLot">
            <div>
                <h5>{lot.lotName}</h5>
            </div>
            <div className="lot-operation">
                <button
                    onClick={handleClick}
                    name='addEnology'
                >Edd enology</button>
                <button
                    onClick={handleClick}
                    name='addRawMaterial'
                >Edd raw material</button>
                <button
                    onClick={handleClick}
                    name='measurement'
                >Measurement</button>
                <button
                    onClick={handleClick}
                    name='grapePress'
                >Grape press</button>
                <button
                    onClick={handleClick}
                    name='sulfurMeasurement'
                >Sulfur measurement</button>
                <button
                    onClick={handleClick}
                    name='transfusion'
                >Transfusion</button>
                <button
                    onClick={handleClick}
                    name='bottling'
                >Bottling</button>
            </div>
            {operation.addEnology && <AddEnologyToLot lot={lot} />}
            {operation.addRawMaterial && <AddRawMaterialToLot lot={lot} />}
            {operation.measurement && <Measurement lot={lot} />}
            {operation.grapePress && <GrapePress lot={lot} />}
            {operation.sulfurMeasurement && <SulfurMeasurement lot={lot} />}
            {operation.transfusion && <Transfusion lot={lot} />}
            {operation.bottling && <AddBottling lot={lot} />}
        </div>

    )
}