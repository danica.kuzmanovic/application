import { FloatingLabel, Form } from 'react-bootstrap'
import { lotTransfusion } from '../../api/lot';
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query"
import { getAllTanks } from '../../api/tank';
import { useNavigate } from 'react-router-dom';

export const Transfusion = ({ lot }) => {

    const navigate = useNavigate()

    const schema = yup.object().shape({
        targetTankId: yup.number().required(),
        date: yup.string().required(),
        comment: yup.string().default(""),
        wasteAmount: yup.number(),
        wineAmount: yup.number()
    })
    const { register,
        handleSubmit,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });

    const { data: tanks = [] } = useQuery(['tanks'], getAllTanks)

    const freeTanks = tanks.filter(tank => {
        if (tank.tankDetailsDto === null) {
            return tank
        }
    })

    const queryClient = useQueryClient()

    const transfusionMutation = useMutation(lotTransfusion, {
        onSuccess: () => {
            queryClient.invalidateQueries(['lots'])
        }
    })
    const handleTransfusion = (transfusionDto) => {

        transfusionMutation.mutate(transfusionDto)
    }


    const onSubmit = (data) => {
        const transfusionDto = {
            targetTankId: data.targetTankId,
            date: data.date,
            lotId: lot.lotId,
            comment: data.comment,
            wasteAmount: data.wasteAmount,
            wineAmount: data.wineAmount
        }
        handleTransfusion(transfusionDto)
        navigate('/lot')
    }

    return (
        <div>
            <Form className="addToLot-form" noValidate onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Waste Amount:">
                        <Form.Control
                            type='number'
                            name='wasteAmount'
                            {...register('wasteAmount')}
                            isInvalid={errors.wasteAmount}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Wine Amount:">
                        <Form.Control
                            type='number'
                            name='wineAmount'
                            {...register('wineAmount')}
                            isInvalid={errors.wineAmount}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Date:">
                        <Form.Control
                            type='date'
                            name='amount'
                            {...register('date')}
                            isInvalid={errors.date}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Target Tank:">
                        <Form.Select
                            {...register('targetTankId')}
                            isInvalid={errors.targetTankId}
                        >
                            <option>choose tank</option>
                            {freeTanks.map((targetTankId) => {
                                return (
                                    <option
                                        value={targetTankId.id}
                                        key={targetTankId.id}> {targetTankId.orderNumber}</option>
                                )
                            })}
                        </Form.Select>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3" >
                    <FloatingLabel controlId="floatingInputGrid" label="Comment:">
                        <Form.Control
                            type='textarea'
                            name='comment'
                            {...register('comment')}
                            isInvalid={errors.comment}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <button type='submit'>Add</button>
                </Form.Group>
            </Form>
        </div>
    )
}