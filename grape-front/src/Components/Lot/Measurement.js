import { Button, FloatingLabel, Form } from 'react-bootstrap'
import { lotMeasurement } from '../../api/lot';
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import { useMutation,useQueryClient } from "@tanstack/react-query"
import { useNavigate } from 'react-router-dom';

export const Measurement = ({lot}) => {

    const navigate = useNavigate()

    const schema = yup.object().shape({
        acid: yup.number().required(),
        alcohol: yup.number().required(),
        date: yup.string().required(),
        pH: yup.number().required(),
        sugar: yup.number().required(),
        temperature: yup.number().required()
        
    })
    const { register,
        handleSubmit,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });


    const queryClient = useQueryClient()

    const measurementMutation = useMutation(lotMeasurement, {
        onSuccess: () => {
            queryClient.invalidateQueries(['lots'])
        }
    })
    const handleMeasurement = (measurementDto) => {

        console.log(measurementDto)
        measurementMutation.mutate(measurementDto)
        navigate('/lot')
    }


    const onSubmit = (data) => {
        const measurementDto = {
            acid: data.acid,
            date: data.date,
            alcohol: data.alcohol,
            lotId: lot.lotId,
            pH: data.pH,
            sugar: data.sugar,
            temperature: data.temperature
        }
        handleMeasurement(measurementDto)
    }

    return (
        <div>
        <Form className="addToLot-form" noValidate onSubmit={handleSubmit(onSubmit)}>
                
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Acid:">
                        <Form.Control
                            type='number'
                            name='acid'
                            {...register('acid')}
                            isInvalid={errors.acid}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Alcohol:">
                        <Form.Control
                            type='number'
                            name='alcohol'
                            {...register('alcohol')}
                            isInvalid={errors.alcohol}
                        />
                    </FloatingLabel>
                </Form.Group>
               
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="pH:">
                        <Form.Control
                            type='number'
                            name='pH'
                            {...register('pH')}
                            isInvalid={errors.pH}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Sugar:">
                        <Form.Control
                            type='number'
                            name='sugar'
                            {...register('sugar')}
                            isInvalid={errors.sugar}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Temperature:">
                        <Form.Control
                            type='number'
                            name='temperature'
                            {...register('temperature')}
                            isInvalid={errors.temperature}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Date:">
                        <Form.Control
                            type='date'
                            name='amount'
                            {...register('date')}
                            isInvalid={errors.date}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <button type='submit'>Add</button>
                </Form.Group>

            </Form>
            </div>
    )
}