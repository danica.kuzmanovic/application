import { useNavigate } from "react-router-dom"
import { useState } from "react"
import { EndLot } from "./EndLot"



export const Lot = ({ lots }) => {

    const [endLotModal, setEndLotModal] = useState(false)

    const navigate = useNavigate()

    const handleEdit = (lot) => {
        navigate(`/lot/edit/${lot.lotId}`, { state: lot })
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Start date</th>
                    <th>Expected end date</th>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                {lots && lots.map((lot) => {
                    return (
                        <tr key={lot.lotId}>
                            <td>{lot.lotName}</td>
                            <td>{lot.startDate}</td>
                            <td>{lot.expectedEndDate}</td>
                            <td>
                                <button className="editLot-btn"
                                    onClick={() => handleEdit(lot)}
                                >
                                    Edit</button>
                                <button className="editLot-btn"
                                    onClick={() => { setEndLotModal(true) }}
                                >End Lot</button>
                                {endLotModal && <EndLot lot={lot} setEndLotModal={setEndLotModal}/>}
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}