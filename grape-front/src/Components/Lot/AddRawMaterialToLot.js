import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { getAllRawMaterials } from '../../api/rawMaterial';
import { yupResolver } from '@hookform/resolvers/yup'
import { Form, FloatingLabel, Button } from 'react-bootstrap'
import { addRawMaterialToLot } from '../../api/lot';
import { useNavigate } from 'react-router-dom';


export const AddRawMaterialToLot = ({ lot }) => {
    const navigate = useNavigate()

    const { data: rawMaterials = [] } = useQuery(['rawMaterials'], getAllRawMaterials);

    const queryClient = useQueryClient();

    const schema = yup.object().shape({
        rawMaterialId: yup.number().required(),
        quantity: yup.number().typeError("").positive("").required(),
        date: yup.string().required()
    })

    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(schema)
    })

    const addRawMaterialToLotMutation = useMutation(addRawMaterialToLot, {
        onSuccess: () => {
            queryClient.invalidateQueries('rawMaterials')
        }
    })

    const onSubmit = (data) => {
        const addRawMaterialToLotDto = {
            lotId: lot.lotId,
            rawMaterialId: data.rawMaterialId,
            quantity: data.quantity,
            date: data.date
        }
        addRawMaterialToLotMutation.mutate(addRawMaterialToLotDto)
        navigate('/lot')
    }

    return (
        <div>
            <Form className="addToLot-form" noValidate onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="RawMaterial:">
                        <Form.Select
                            {...register('rawMaterialId')}
                            isInvalid={errors.enology}
                        >
                            <option>choose raw material</option>
                            {rawMaterials.map((rawMaterial) => {
                                return (
                                    <option
                                        value={rawMaterial.id}
                                        key={rawMaterial.id}> {rawMaterial.name}</option>
                                )
                            })}
                        </Form.Select>
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Quantity:">
                        <Form.Control
                            type='number'
                            name='quantity'
                            {...register('quantity')}
                            isInvalid={errors.quantity}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <FloatingLabel controlId="floatingInputGrid" label="Date:">
                        <Form.Control
                            type='date'
                            name='amount'
                            {...register('date')}
                            isInvalid={errors.date}
                        />
                    </FloatingLabel>
                </Form.Group>
                <Form.Group className="m-3">
                    <button type='submit'>Add</button>
                </Form.Group>

            </Form>
        </div>
    )
}