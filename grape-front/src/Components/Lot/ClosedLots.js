import { getPdf } from "../../api/pdf"

export const ClosedLots = ({ closedLots }) => {

    const handleDownload = (id) => {
        console.log(id)
        getPdf(id)
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>End date</th>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                {closedLots && closedLots.map((lot) => {
                    return (
                        <tr key={lot.lotId}>
                            <td>{lot.lotName}</td>
                            <td>{lot.endDate}</td>
                            <td>
                                <button className="editLot-btn"
                                    onClick={() => handleDownload(lot.lotId)}
                                >Download report</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}