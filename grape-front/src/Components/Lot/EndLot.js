import { useMutation, useQueryClient } from '@tanstack/react-query'
import { useState } from 'react';
import { endLot } from '../../api/lot';
import '../../css/lot.css'
export const EndLot = ({ lot, setEndLotModal }) => {

    const queryClient = useQueryClient();

    const date = new Date().toJSON().slice(0, 10)

    const [endLotDto, setEndLotDto] = useState({
        comment: "",
        date: date,
        lotId: lot.lotId
    })



    const handleOnChange = (event) => {
        const { name, value } = event.target;
        setEndLotDto(prevEndLotDto => ({
            ...prevEndLotDto,
            [name]: value
        }))
    }

    const endLotMutation = useMutation(endLot, {
        onSuccess: () => {
            queryClient.invalidateQueries('lots')
            queryClient.invalidateQueries('closedLots')
        }
    })

    const handleEndLot = () => {
        endLotMutation.mutate(endLotDto)
        setEndLotModal(false)
    }

    return (
        <div className="modal">
            <div className="modal-container">
                <div className="modal-title">
                    <h3>{lot.lotName}</h3>
                </div>
                <div className="modal-body">
                    <div className='modal-input'>
                        <label>Date:</label>
                        <input
                            type='date'
                            name='date'
                            onChange={handleOnChange}
                            required
                        />
                    </div>
                    <div className='modal-input'>
                        <label>Comment:</label>
                        <input
                            type='textarea'
                            name='comment'
                            onChange={handleOnChange}
                        />
                    </div>
                </div>
                <div className="modal-footer">
                    <button className='modal-btn'
                        onClick={() => setEndLotModal(false)}
                    >Cancel</button>
                    <button
                        className='modal-btn'
                        type='submit'
                        onClick={handleEndLot}
                    >End Lot</button>
                </div>
            </div>
        </div>
    )
}