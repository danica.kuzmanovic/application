import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { FloatingLabel, Form } from 'react-bootstrap';
import * as yup from 'yup';
import { lotAddBottling } from '../../api/lot';
import { getAllPackaging } from '../../api/packaging';
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';

export const AddBottling = ({ lot }) => {

    const [q, setq] = useState(false);

    const navigate = useNavigate()

    const schema = yup.object().shape({
        date: yup.string().required(),
        packagingId: yup.number().required(),
        quantity: yup.number().required()
    })

    const { register,
        handleSubmit,
        formState: { errors } } = useForm({
            resolver: yupResolver(schema)
        });

    const queryClient = useQueryClient();

    const bottlingMutation = useMutation(lotAddBottling, {
        onSuccess: () => {
            queryClient.invalidateQueries('packagings')
            navigate('/lot')
        },
        onError: (error) => {
            if (error.response.status)
                return setq(true)
        }

    })

    const { data: bottles = [] } = useQuery(['packagings'], getAllPackaging)

    const bottleDtos = bottles.bottleDtos && bottles.bottleDtos.map((bottle) => {
        const packaging = bottles.packagingDtos && bottles.packagingDtos.filter((packaging) => {
            return (packaging.id === bottle.packagingId)
        })

        return {
            id: packaging[0].id,
            name: packaging[0].name,
            volume: bottle.volume,
            volumeUnit: bottle.volumeUnit === 0 && 'ml',
        }
    })

    const onSubmit = (data) => {
        const bottlingDto = {
            date: data.date,
            packagingId: data.packagingId,
            quantity: data.quantity,
            lotId: lot.lotId
        }

        bottlingMutation.mutate(bottlingDto)

    }
    console.log(lot)

    return (
        <Form className="addToLot-form" noValidate onSubmit={handleSubmit(onSubmit)}>
            {q && <label>Capacity for bottling is {lot.usedCapacity}</label>}
            <Form.Group className="m-3">
                <FloatingLabel controlId="floatingInputGrid" label="Bottle:">
                    <Form.Select
                        {...register('packagingId')}
                        isInvalid={errors.packagingId}
                    >
                        <option>choose bottle</option>
                        {bottleDtos && bottleDtos.map((bottle) => {
                            return (
                                <option
                                    value={bottle.id}
                                    key={bottle.id}> {bottle.name}-{bottle.volume}{bottle.volumeUnit}</option>
                            )
                        })}
                    </Form.Select>
                </FloatingLabel>
            </Form.Group>
            <Form.Group className="m-3">
                <FloatingLabel controlId="floatingInputGrid" label="Quantity:">
                    <Form.Control
                        type='number'
                        name='quantity'
                        {...register('quantity')}
                        isInvalid={errors.quantity}
                    />
                </FloatingLabel>
            </Form.Group>
            <Form.Group className="m-3">
                <FloatingLabel controlId="floatingInputGrid" label="Date:">
                    <Form.Control
                        type='date'
                        name='amount'
                        {...register('date')}
                        isInvalid={errors.date}
                    />
                </FloatingLabel>
            </Form.Group>
            <Form.Group className="m-3">
                <button type='submit'>Add</button>
            </Form.Group>
        </Form>
    )
}