import grappeApi from "./grappe"

export const getAllTanks = async () => {
    const response = await grappeApi.get('/tank');
    return response.data;
}

export const addTank = async (tank) => {
    return grappeApi.post('/tank', tank)
}

export const deleteTank = async (id) => {
    return grappeApi.delete(`/tank/${id}`)
}