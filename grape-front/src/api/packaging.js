import grappeApi from "./grappe"

export const getAllPackaging = async () => {
    const responsive = await grappeApi.get("/packaging");
    return responsive.data;
}

export const updatePackagingQuantity = async (packaging) => {
    return await grappeApi.patch('/packaging/changeQuantity', packaging)
}


export const deletePackaging = async (id) => {
    return await grappeApi.delete(`/packaging/${id}`)
}

export const addPackaging = async (packaging) => {
    return await grappeApi.post('/packaging', packaging)
}

export const addPackagingBottle = async (bottle) => {
    return await grappeApi.post('/packaging/bottle', bottle)
}