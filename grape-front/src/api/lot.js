import grappeApi from "./grappe";


export const getActiveLots = async () => {
    const response = await grappeApi.get('/lot/activeLots')
    return response.data.lots;
}

export const getClosedLots = async () => {
    const response = await grappeApi.get('/lot/closedLots')
    return response.data.closedLotList;
}

export const addEnologyToLot = async (enologyToLot) => {
    return await grappeApi.post('/lot/addEnologyToLot', enologyToLot)
}


export const addRawMaterialToLot = async (rawMaterialToLot) => {
    return await grappeApi.post('/lot/addRawMaterialToLot', rawMaterialToLot)

}

export const getLotDetails = async (id) => {
    return await grappeApi.get(`/lot/details/${id}`)
}

export const lotAddBottling = async (bottlingDto) => {
    return await grappeApi.post('/lot/bottling', bottlingDto);
}

export const lotGrapePress = async (grapePressDto) => {
    return await grappeApi.post('/lot/grapePressing', grapePressDto);
}

export const lotMeasurement = async (measurementDto) => {
    return await grappeApi.post('/lot/measurement', measurementDto);
}
export const lotSulfurMeasurement = async (sulfurMeasurementDto) => {
    return await grappeApi.post('/lot/sulfurMeasurement', sulfurMeasurementDto);
}
export const lotTransfusion = async (transfusionDto) => {
    return await grappeApi.post('/lot/transfusion', transfusionDto);
}

export const endLot = async (endLotDto) => {
    return await grappeApi.post('lot/endLot', endLotDto)
}

export const openLot = async (openLotDto) => {
    return await grappeApi.post('lot/openLot', openLotDto)

}
