import grappeApi from "./grappe"


export const getAllRawMaterials = async () => {
    const response = await grappeApi.get('/rawMaterial');
    return response.data;
}

export const deleteRawMaterial = async (id) => {
    return await grappeApi.delete(`/rawMaterial/${id}`);
}

export const addRawMaterial = async (rawMaterial) => {
    return await grappeApi.post('/rawMaterial', rawMaterial);
}

export const updateRawMaterialQuantity = async (item) => {
    return await grappeApi.patch('/rawMaterial/changeQuantity', item);
}