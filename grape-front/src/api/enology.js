import {grappeApi } from './grappe'

export const getAllEnology = async () => {
    const response = await grappeApi.get('/enology')
    return response.data;
}

export const addEnology = async (enology) => {
    return await grappeApi.post("/enology", enology);
}

export const deleteEnology = async (id) => {
    return await grappeApi.delete(`/enology/${id}`)
}

export const updateEnologyQuantity = async (item) => {
    return await grappeApi.patch("/enology/changeQuantity", item)
}

export const getAllCategories = async () => {
    const response = await grappeApi.get('enology/categories')
    return response.data;
}

export const addEnologyCategory = async (category) => {
    return await grappeApi.post('/enology/category', category)
}
