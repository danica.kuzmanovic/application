import grappeApi from "./grappe"

export const getPdf = async (id) => {
    grappeApi.get(`/report/download/${id}`, {
        responseType: 'blob', // important
    }).then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `${id}.pdf`); //or any other extension
        document.body.appendChild(link);
        link.click();
    });
}