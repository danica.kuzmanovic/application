import { Link } from "react-router-dom"
import './css/menu.css'
// import { Nav, Navbar, NavDropdown } from 'react-bootstrap'
export const Menu = () => {
    return (
        <div className="app-menu">
            <div className="app-menu-item">
                <label>Enology</label>
                <Link className="app-menu-item-link" to='/enology'>Enology list</Link>
                <Link className="app-menu-item-link" to='/enology/new'>Add Enology</Link>
            </div>
            <div className="app-menu-item">
                <label>Raw material</label>
                <Link className="app-menu-item-link" to='/rawMaterial'>Raw material list</Link>
                <Link className="app-menu-item-link" to='/rawMaterial/new'>Add Raw material</Link>
            </div>
            <div className="app-menu-item">
                <label>Packaging</label>
                <Link className="app-menu-item-link" to='/packaging'>Packaging list</Link>
                <Link className="app-menu-item-link" to='/packaging/newPackaging'>Add Packaging</Link>
                <Link className="app-menu-item-link" to='/packaging/newPackagingBottle'>Add Bottle</Link>
            </div>
            <div className="app-menu-item">
                <label>Tank</label>
                <Link className="app-menu-item-link" to='/tank'>Tank list</Link>
                <Link className="app-menu-item-link" to='/tank/new'>Add Tank</Link>
            </div>
            <div className="app-menu-item">
                <label>Lot</label>
                <Link className="app-menu-item-link" to='/lot'>Lot list</Link>
                <Link className="app-menu-item-link" to='/openLot'>Open Lot</Link>
            </div>
        </div >
    )
}