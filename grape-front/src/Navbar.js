import { Link } from 'react-router-dom'
import { IoMenuOutline } from 'react-icons/io5'
import './css/navbar.css'

export const Navbar = ({ menuToggle, setMenuToggle }) => {

    const openMenu = () => {
        return setMenuToggle(!menuToggle)
    }

    return (
        <div className='grape-navbar'>
            <IoMenuOutline
                role='button'
                className='menu-svg'
                onClick={openMenu}
            />
            <Link className='grape-navbar-link' to='/'>Home</Link>
        </div>

    )
}